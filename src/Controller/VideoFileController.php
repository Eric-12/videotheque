<?php

namespace App\Controller;

use App\Entity\VideoFile;
use App\Form\VideoFileType;
use App\Form\EncodageffmpegType;
use App\Service\EncoderVideoService;
use App\Repository\VideoFileRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

#[Route('/film')]
class VideoFileController extends AbstractController
{
    #[Route('/', name: 'app_video_file_index', methods: ['GET'])]
    public function index(VideoFileRepository $videoFileRepository, PaginatorInterface $paginator, Request $request): Response
    {

        // $allFilesVideo = $videoFileRepository->findBy([], ['nom' => 'ASC']);
        // $fileVideoExist = [];
        // foreach ($allFilesVideo as $key => $entity) {
        //     if (file_exists("D:\FILMS" . $entity->getPath())) {
        //         $fileVideoExist[] = $entity;
        //     } 
        // }

        // CONFIGURER ET PERSONALISER POUR LE PAGINATEUR
        $pagination = $paginator->paginate(
            $videoFileRepository->findBy([], ['nom' => 'ASC']),
            $request->query->getInt('page', 1),
            100
        );
        $pagination->setCustomParameters([
            'align'   => 'center', // 'left', 'center', or 'right'
            'size'    => 'medium',  // 'small', 'medium', or 'large'
            'rounded' => true,
        ]);



        return $this->render('video_file/index.html.twig', [
            'pagination' => $pagination,
        ]);
    }

    #[Route('/new', name: 'app_video_file_new', methods: ['GET', 'POST'])]
    public function new(Request $request, VideoFileRepository $videoFileRepository): Response
    {
        $videoFile = new VideoFile();
        $form = $this->createForm(VideoFileType::class, $videoFile);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $videoFileRepository->save($videoFile, true);

            return $this->redirectToRoute('app_video_file_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('video_file/new.html.twig', [
            'video_file' => $videoFile,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_video_file_show', methods: ['GET', 'POST'])]
    public function show(VideoFile $videoFile): Response
    {
        return $this->render('video_file/show.html.twig', [
            'url_server_video' => $this->getParameter('url_Sever_video'),
            'video_file' => $videoFile,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_video_file_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, VideoFile $videoFile, VideoFileRepository $videoFileRepository): Response
    {
        $form = $this->createForm(VideoFileType::class, $videoFile);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $videoFileRepository->save($videoFile, true);

            return $this->redirectToRoute('app_video_file_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('video_file/edit.html.twig', [
            'url_server_video' => $this->getParameter('url_Sever_video'),
            'video_file' => $videoFile,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_video_file_delete', methods: ['POST'])]
    public function delete(Request $request, VideoFile $videoFile, VideoFileRepository $videoFileRepository): Response
    {
        if ($this->isCsrfTokenValid('delete' . $videoFile->getId(), $request->request->get('_token'))) {
            $videoFileRepository->remove($videoFile, true);
        }

        return $this->redirectToRoute('app_video_file_index', [], Response::HTTP_SEE_OTHER);
    }

    #[Route('/{id}/ffmpeg/encoder', name: 'app_video_file_ffmpeg_encoder', methods: ['GET', 'POST'])]
    public function ffmpegEncoder(Request $request, VideoFile $videoFile, EncoderVideoService $encoderVideoService): Response
    {
        $fileInfoFFmpeg = $encoderVideoService->infoVideo($videoFile);

        $encodageffmpegType = $this->createForm(EncodageffmpegType::class, $fileInfoFFmpeg);
        $encodageffmpegType->handleRequest($request);

        if ($encodageffmpegType->isSubmitted() && $encodageffmpegType->isValid()) {
            $data = $encodageffmpegType->getData();
            $output = $encoderVideoService->encodeToMp4($videoFile, $data);
            $this->addFlash('ffmpeg','Encodage terminé !');
            return $this->redirectToRoute('app_video_file_index', [], Response::HTTP_SEE_OTHER);

            return $this->render('video_file/ffmpeg_encode.html.twig', [
                'output' => $output,
                'video_file' => $videoFile,
                'encodageffmpegType' => $encodageffmpegType,
            ]);
        }


        return $this->render('video_file/ffmpeg_encode.html.twig', [
            'fileInfoFFmpeg' => $fileInfoFFmpeg,
            'video_file' => $videoFile,
            'encodageffmpegType' => $encodageffmpegType,
        ]);
         
    }

    #[Route('/ffmpeg/log', name: 'app_video_file_ffmpeg_log', methods: ['GET'])]
    public function ffmpegLog(): Response
    {
        return $this->render('video_file/log.html.twig', []);
    }


    #[Route('/ffmpeg/get-log', name: 'app_video_file_ffmpeg_get_log', methods: ['GET'])]
    public function ffmpegGetLog(): Response
    {
        $filename = $this->getParameter('ffmpeg_log_directory') . '/ffmpeg.log';

        $logFile = fopen($filename, "r");
        $result = fread($logFile, filesize($filename));
        fclose($logFile);

        return new JsonResponse($result);

        // $response = new Response(
        //     $result,
        //     Response::HTTP_OK,
        //     ['content-type' => 'text/html']
        // );
        // return  $response;

        // return new JsonResponse(['log' => $result]);
    }

    
}
