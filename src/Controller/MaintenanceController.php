<?php

namespace App\Controller;

use App\Entity\Film;
use App\Entity\Genre;
use App\Entity\Acteur;
use App\Entity\VideoFile;
use App\Entity\Realisateur;
use App\Form\AllFilesVideoType;
use App\Form\FicheCineDoublonType;
use App\Repository\FilmRepository;
use App\Service\CleanerPathService;
use App\Form\RealisateurSansLienType;
use App\Form\FicheCineNonDescriptiveType;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

#[Route('/maintenance')]
class MaintenanceController extends AbstractController
{
    #[Route('/cleaner/database', name: 'app_maintenance_cleaner_database_index', methods: ['GET', 'POST'])]
    public function index(ManagerRegistry $doctrine, Request $request): Response
    {
        $ficheCineDoublonType = $this->createForm(FicheCineDoublonType::class);
        $ficheCineDoublonType->handleRequest($request);
        if ($ficheCineDoublonType->isSubmitted() && $ficheCineDoublonType->isValid()) {
            // Récupère les données du formulaire
            $formData = $ficheCineDoublonType->getData();
            $entityManager = $doctrine->getManager();
            foreach ($formData['titre'] as $key => $entity) {
                $entityManager->remove($entity);
                $entityManager->flush();
            }
            foreach ($formData['realisateur'] as $key => $entity) {
                $entityManager->remove($entity);
                $entityManager->flush();
            }
            foreach ($formData['acteur'] as $key => $entity) {
                $entityManager->remove($entity);
                $entityManager->flush();
            }
            $this->addFlash('notice', 'La base de données a été nettoyé !');
            // Rafraichir la page
            return $this->redirectToRoute('app_maintenance_cleaner_database_index');
        }

        $realisateurSansLienForm = $this->createForm(RealisateurSansLienType::class);
        $realisateurSansLienForm->handleRequest($request);
        if ($realisateurSansLienForm->isSubmitted() && $realisateurSansLienForm->isValid()) {
            // Récupère les données du formulaire
            $formData = $realisateurSansLienForm->getData();
        }

        // Formulaire de nettoyage pour la base de données 
        // Rechercher et proposer la suppression des enregistrement ne contenant ni résume ni date de sortie ni affiches
        $ficheCineNonDescriptiveType = $this->createForm(FicheCineNonDescriptiveType::class);
        $ficheCineNonDescriptiveType->handleRequest($request);
        if ($ficheCineNonDescriptiveType->isSubmitted() && $ficheCineNonDescriptiveType->isValid()) {
            // Récupère les données du formulaire
            $formData = $ficheCineNonDescriptiveType->getData();
            // Service Doctrine qui nous permet de manipuler des entités
            $entityManager = $doctrine->getManager();
            // Suppression des enregistrements 
            foreach ($formData['filter'] as $key => $entity) {
                $entityManager->remove($entity);
                $entityManager->flush();
            }
            // Message a affiché lors du chargement du template 
            $this->addFlash('notice', 'La base de données a été nettoyé !');
            return $this->redirectToRoute('app_maintenance_cleaner_database_index');
        }

        // Affichage de la page
        return $this->render('maintenance/cleaner_database.html.twig', [
            'ficheCineNonDescriptiveType' => $ficheCineNonDescriptiveType,
            // 'realisateurSansLienForm' => $realisateurSansLienForm,
            'ficheCineDoublonType' => $ficheCineDoublonType,
        ]);
    }

    #[Route('/cleaner/drop-db', name: 'app_maintenance_cleaner_database_drop_db', methods: ['GET'])]
    public function dropDatabase(ManagerRegistry $doctrine)
    {
        $entityManager = $doctrine->getManager();

        // FILMS
        $film = $doctrine->getRepository(Film::class)->findAll();
        foreach ($film as $entity) {
            $entityManager->remove($entity);
        }
        $entityManager->flush();

        // GENRE
        $genre = $doctrine->getRepository(Genre::class)->findAll();
        foreach ($genre as $entity) {
            $entityManager->remove($entity);
        }
        $entityManager->flush();

        // REALISATEUR
        $realisateur = $doctrine->getRepository(Realisateur::class)->findAll();
        foreach ($realisateur as $entity) {
            $entityManager->remove($entity);
        }
        $entityManager->flush();

        // ACTEURS
        $acteurs = $doctrine->getRepository(Acteur::class)->findAll();
        foreach ($acteurs as $entity) {
            $entityManager->remove($entity);
        }
        $entityManager->flush();

        $this->addFlash(
            'notice',
            count($film) . ' fiches de films ont été supprimées.'
        );
        return $this->redirectToRoute('app_home_page_index');
    }

    #[Route('/cleaner/path/poster', name: 'app_cleaner_path_poster', methods:['GET','POST'])]
    public function dropPoster(FilmRepository $filmRepository, CleanerPathService $cleanerPathService): Response
    {
        $ListeDesFichiersDuDossier = $cleanerPathService->ListeDesFichiersDuDossier($this->getParameter('affiche_directory'));
        $fichierOrphelin = [];
        if(count($ListeDesFichiersDuDossier) > 0) {
            foreach ($ListeDesFichiersDuDossier as $fichier) {
                if ($filmRepository->findOneBy(['afficheFilename' => $fichier]) === null) $fichierOrphelin[] = $fichier;
            }
        }

        //Liste des fichiers orphelin
        $form = $this->createFormBuilder()
            ->setAction($this->generateUrl('app_cleaner_path_poster_delete'))
            ->add('allSelected',CheckboxType::class,[
                'label' => 'Tout cocher',
                'row_attr' => ['class' =>'d-flex flex-column text-bg-light p-2 mb-3'],
                'required' => false,
            ])
            ->add('affiches', ChoiceType::class, [
                'label' => false,
                'choices' => $fichierOrphelin,
                'choice_label' => function ($choice, $key, $value) {
                    return $value;
                },
                // Gérer la selection des items.
                'multiple' => true,
                'expanded' => true,
            ])
            ->add('save', SubmitType::class, ['label' => 'Supprimer les fichiers sélectionnés !'])
            ->getForm()
        ;

        return $this->render('maintenance/drop_poster.html.twig', [
            'form' => $form,
        ]);
    }


    #[Route('/cleaner/path/poster/delete', name: 'app_cleaner_path_poster_delete')]
    public function deletePosters(Request $request, CleanerPathService $cleanerPathService)
    {
        // $f = $this->isCsrfTokenValid('', $request->get('form')['_token']);
        $form = $request->get('form');
        if($form && count($form['affiches']) > 0) {
            $cleanerPathService->supprimeLesAffiches($this->getParameter('affiche_directory'), $form['affiches']);
            $this->addFlash("notice", "Les fichiers sélectionnés ont été supprimées !");
        }
        return $this->redirectToRoute('app_fiche_cinema_index');
    }
}
