<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


#[Route('/request-example')]
class RequestExampleController extends AbstractController
{
     #[Route( '/', name: 'app_request', methods: ["GET"] )]
     public function index(Request $request): Response
     {
          return $this->render('request_example/index.html.twig',[
               'methode' => $request->getMethod(),
               'ip' =>  $request->getClientIp(),
               'content_type' => $request->headers->get('content-type'),
               'headers' => $request->headers->all(),
               'params' =>  $request->query->all() // Equivalent of $_GET 
          ]);
     }


     /*  Requête GET avec (query params) données transmisent dans l'url.
          Exemple de requête : http://localhost:8000/request-example/get/with-query-params?numPage=20005&checked=false
     */
     #[Route( '/get/with-query-params', name: 'app_request_example_with_query_params', methods: ["GET"] )]
     public function get(Request $request): Response
     {
          $headers = $request->headers->all();
          ksort($headers);

          return $this->render('request_example/index.html.twig',[
               'methode' =>$request->getMethod(),
               'ip' =>  $request->getClientIp(),
               'content_type' => $request->headers->get('content-type'),
               'headers' => $headers,
               'params' => $request->query->all(), // Equivalent of $_GET 
          ]);
     }


     /*  Requête GET avec séparateur(/) données transmisent dans l'url.
          Exemple de requête : http://localhost:8000/request-example/get/params/20006/true
     */
     #[Route( '/get/params/{numPage}/{checked}', name: 'app_request_example_params', methods: ["GET"] )]
     public function params(Request $request, string $numPage, string $checked ): Response
     {
          $headers = $request->headers->all();
          ksort($headers);

          return $this->render('request_example/index.html.twig',[
               'methode' =>$request->getMethod(),
               'ip' =>  $request->getClientIp(),
               'content_type' => $request->headers->get('content-type'),
               'headers' => $headers,
               'params' => [$numPage, $checked] // Equivalent of $_GET 
          ]);
     }

   
     /*   Requête POST via un formulaire
          Les formulaires HTML proposent trois méthodes d'encodage . 
               - application/x-www-form-urlencoded(le défaut)
               - multipart/form-data
               - text/plain
     */

     /*
          application/x-www-form-urlencoded(le défaut) ou multipart/form-data
          Exemple de requête : http://localhost:8000/request-example/post/with-form-data
     */
     #[Route( '/post/formulaire', name: 'app_scraping_post_formulaire', methods: ["POST"] )]
     public function post(Request $request): Response
     {
          $numPage = $request->request->get('numPage');
          $numChecked = $request->request->get('checked');

          $headers = $request->headers->all();
          ksort($headers);

          return $this->render('request_example/index.html.twig',[
               'methode' =>$request->getMethod(),
               'ip' =>  $request->getClientIp(),
               'content_type' => $request->headers->get('content-type'),
               'headers' => $headers,
               'params' => $request->request->all() // Equivalent of $_POST
          ]);
     }


     /*   Requête GET avec données transmisent dans le corps de la requête
          Exemple de requête : http://localhost:8000/request-example/post/data-transmitted-in-the-bodyy
          BODY / JSON 
          {
               "numPage":"20004",
               "checked": "false"
          }
     */
     #[Route( '/post/json', name: 'app_scraping_post_json', methods: ["POST"] )]
     public function dataTransmittedInTheBody(Request $request): Response
     {
          $headers = $request->headers->all();
          ksort($headers);

          $content = $request->getContent(); // Récupères les données transmisent dans le corps de la requête HTTP
          $data = $request->toArray(); // Convertit en tableau une chaîne JSON
     
          $reponse = [
               'method'=> $request->getMethod(), 
               'ip' =>  $request->getClientIp(), 
               'content_type' => $request->headers->get('content-type'),
               'params' =>  $request->toArray(),
               'headers' => $headers,
          ];

          return $this->json($reponse , 200, []);
     }
}
