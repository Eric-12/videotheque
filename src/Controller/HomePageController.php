<?php

namespace App\Controller;

use App\Entity\Film;
use App\Form\TitreFilmType;
use App\Form\MultiFilterFilmType;
use App\Repository\FilmRepository;
use Doctrine\Persistence\ManagerRegistry;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class HomePageController extends AbstractController
{
    #[Route('/', name: 'app_home_page_index', methods: ['GET', 'POST'])]
    public function index(Request $request, ManagerRegistry $doctrine, PaginatorInterface $paginator, FilmRepository $filmRepository): Response
    {
        // Initialisation des valeurs des filtres par défaut.
        // Si le formulaire 
        $date = (string) "1899|" . date("Y");
        $score = "0|5";
        $duree = "1|255";

        $chercheTitreFilmForm = $this->createForm(TitreFilmType::class);
        $chercheTitreFilmForm->handleRequest($request);
        if ($chercheTitreFilmForm->isSubmitted() && $chercheTitreFilmForm->isValid()) {
            $search = $chercheTitreFilmForm->getData();
            $ficheCine = $filmRepository->findByTitle($search['filterTitle']);
        }

        $multiFilterFilmForm = $this->createForm(MultiFilterFilmType::class);
        $multiFilterFilmForm->handleRequest($request);
        if ($multiFilterFilmForm->isSubmitted() && $multiFilterFilmForm->isValid()) {
            $date = $multiFilterFilmForm->get('date')->getData();
            $score = $multiFilterFilmForm->get('score')->getData();
            $duree = $multiFilterFilmForm->get('duree')->getData();
            $genre = $multiFilterFilmForm->get('genre')->getData();
            $realisateur = $multiFilterFilmForm->get('realisateur')->getData();
            $acteur = $multiFilterFilmForm->get('acteur')->getData();

            $ficheCine = $filmRepository->findByFilter($date, $score, $duree, $genre, $realisateur, $acteur);
        }

        if (!$chercheTitreFilmForm->isSubmitted() && !$multiFilterFilmForm->isSubmitted()) {
            $ficheCine = $doctrine->getRepository(Film::class)->findAll();
        }

        // CONFIGURER ET PERSONALISER POUR LE PAGINATEUR
        $ficheCinePaginator = $paginator->paginate(
            $ficheCine,
            $request->query->getInt('page', 1),
            20
        );
        $ficheCinePaginator->setCustomParameters([
            'align'   => 'center', // 'left', 'center', or 'right'
            'size'    => 'medium',  // 'small', 'medium', or 'large'
            'rounded' => true,
        ]);

        // Affiche les données d'extarction dans la page web
        return $this->render('home_page/index.html.twig', [
            'ficheCinePaginator' => $ficheCinePaginator,
            'chercheTitreFilmForm' => $chercheTitreFilmForm,
            'multiFilterFilmForm' => $multiFilterFilmForm,
            'date' => $date,
            'score' => $score,
            'duree' => $duree,
        ]);
    }
}
