<?php

namespace App\Controller;

use App\Entity\VideoFile;
use App\Form\AllFilesVideoType;
use App\Form\FileSearchVideoType;
use App\Repository\VideoFileRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;

#[Route('/stockage')]
class StockageController extends AbstractController
{
    #[Route('/file-search', name: 'app_stockage_scan_disk')]
    public function parcourirDossier(Request $request, ManagerRegistry $doctrine): Response
    {
        // PREMIERE ETAPE:Formulaire pour sélectionner un dossier (recherche de films)
        $FileSearchVideo = $this->createForm(FileSearchVideoType::class);
        $FileSearchVideo->handleRequest($request);

        // PREMIERE ETAPE: Renvoi la listes des films en parcourant les sous dossiers.
        if ($FileSearchVideo->isSubmitted() && $FileSearchVideo->isValid()) {
            $dir = $FileSearchVideo->get('dir')->getData();
            // Parcours le dossier et les sous dossier à la recherche de films
            $path = $this->scanDisk(str_replace('\\', '/',$dir));
            // Formulaire contenant une entrée pour chaque films sous forme de checkBox
            $allFilesVideo = $this->createForm(AllFilesVideoType::class, ['dir' => $dir, 'listeDesFilms' => $path]);
            return $this->render('stockage/scanDisk.html.twig', [
                'path' => $path,
                'FileSearchVideo' => $FileSearchVideo,
                'allFilesVideo' => $allFilesVideo
            ]);
        }

        // DEUXIEME ETAPE:
        $allFilesVideo = $request->get('all_files_video');
        if ($allFilesVideo) {
            $entityManager = $doctrine->getManager();
            foreach ($allFilesVideo['path'] as $key => $pathVideo) {

                // Supprime la racine du chemin d'acces (serveur)
                $numberCharInPath = strlen($allFilesVideo['dir']);
                $fileVideoPath = substr( $pathVideo, $numberCharInPath + 1);

                // NOM DU FICHIER SANS L'EXTENSION
                // Scinde la chaîne de caractères (le chemin d'accès au fichier) en segments (array)
                $splitPath = explode('/', $pathVideo);
                // Récupère le dernier élément du tableau (nom du fichier avec l'extension)
                $fileNameWithExtension = end($splitPath);
                // Scinde la chaîne de caractères (nom du fichier suivi de son extension) en segments (array)
                $splitFile = explode('.', $fileNameWithExtension);
                // Retire ou déplile le dernier élément du tableau (l'extension)
                array_pop($splitFile);
                // Rassemble les éléments du tableau en une chaîne
                $fileVideoFileName = implode(' ', $splitFile);

                // Sauvegarde dans la Database
                $fileVideo = new VideoFile;
                $fileVideo
                    ->setRoot( str_replace('\\', '/', $allFilesVideo['dir']) )
                    ->setpath($fileVideoPath)
                    ->setNom($fileVideoFileName)
                    ->setTaille(filesize($pathVideo));
                $entityManager->persist($fileVideo);
            }
            $entityManager->flush();
            return $this->redirectToRoute('app_video_file_index');
        }

        return $this->render('stockage/selectDir.html.twig', [
            'FileSearchVideo' => $FileSearchVideo,
        ]);
    }


    // Parcours les dossier à la recherche de fichier vidéo dans une fonction recursive
    private function scanDisk($path)
    {
        $arrayfilePath = [];
        if (is_dir($path)) {
            $dir = opendir($path);
            while ($contentDir = readdir($dir)) {
                if ($contentDir != '.' && $contentDir != '..') {
                    if (is_dir($path . '/' . $contentDir)) {
                        $arrayfilePath = array_merge($arrayfilePath, $this->scanDisk($path . '/' . $contentDir));
                    } else {
                        if ($this->isTypeVideo($contentDir)) $arrayfilePath[] = $path . '/' . $contentDir;
                    }
                }
            }
            closedir($dir);
        }
        return $arrayfilePath;
    }


    private function isTypeVideo($file)
    {
        $extentionVideo = ['avi', 'mp4', 'm4v', 'mov', 'flv', 'wmv ', 'asf', 'mpeg', 'mpg', 'vob', 'mkv', 'ts'];
        $fileExtention = pathinfo($file, PATHINFO_EXTENSION);
        return in_array($fileExtention, $extentionVideo);
    }


    #[Route('/show-video/{id}', name: 'app_stockage_show_video')]
    public function showVideo(VideoFileRepository $videoFileRepository, $id): Response
    {
        $filepath = 'D:\\FILMS\\' . $videoFileRepository->find($id)->getPath();
        $executable = '"C:\Program Files (x86)\VideoLAN\VLC\vlc.exe"';

        exec($executable . ' ' . '"' . $filepath . '"');

        return $this->redirectToRoute('app_video_file_index');
    }

}
