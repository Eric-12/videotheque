<?php

namespace App\Controller;

use App\Entity\Film;
use App\Repository\FilmRepository;
use Doctrine\Persistence\ManagerRegistry;
use App\Service\FileUploader;
use App\Service\WebScrapingService;
use App\Form\CollectionFilmType;
use App\Form\ListeDesFichiersVideoNonLierAUneFicheCinemaType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


#[Route('/scraping')]
class WebScrapingController extends AbstractController
{

    #[Route('/', name: 'app_scraping_index')]
    public function index(ManagerRegistry $doctrine): Response
    {
        // Renvoie le nombre de fiches cinéma
        $nombreDeFilm = count($doctrine->getRepository(Film::class)->findAll());

        // Affichage de la page
        return $this->render('scraping/index.html.twig', [
            'nombreDeFilm' => $nombreDeFilm,
        ]);
    }


    #[Route('/auto-extract-data-from-web-page/{numeroPageWeb}/{isSaveDatabase}', name: 'app_scraping_auto_extract_data_from_web_page')]
    public function automateExtractionDataFromWebPage(ManagerRegistry $doctrine, 
                                                    FileUploader $fileUploader,
                                                    ValidatorInterface $validator,
                                                    WebScrapingService $webScrapingService,
                                                    int $numeroPageWeb, 
                                                    $isSaveDatabase = "false"): JsonResponse
    {
        // DEBUT DE L'EXTRACTION DES DONNEES
        $dataWebPage = $webScrapingService->extraireFicheCineDePageWeb($numeroPageWeb);
        // Si des données ont été extraites.
        if (!is_null($dataWebPage)) {
            if ($isSaveDatabase == "true") {
                // Periste les données dans la DB
                $isAddedToDatabase = $webScrapingService->saveDatafromWebPageToDatabase($dataWebPage, $doctrine, $fileUploader, $validator);
                if ($isAddedToDatabase) {
                    return new JsonResponse(['isExtracted' => true, 'data' =>  $dataWebPage]);
                }
                return new JsonResponse(['isExtracted' => false, 'message' => 'Les données de cette page ont déja été extraite.']);
            }
            return new JsonResponse(['isExtracted' => true, 'data' =>  $dataWebPage]);
        }
        return new JsonResponse(['isExtracted' => false, 'message' => 'La page demandée n\'existe pas']);
    }


    #[Route('/liste-des-fichiers-video', name: 'app_liste_fichiers_video')]
    public function searchCorrespondanceEntreTitreFilmEtNomDeFichier(Request $request, FilmRepository $filmRepository): Response
    {
        // Renvoie la liste des fichiers Video non lier à une fiche cinema
        $listeDesFichiersVideoNonLierAUneFicheCinemaType = $this->createForm(ListeDesFichiersVideoNonLierAUneFicheCinemaType::class);
        $listeDesFichiersVideoNonLierAUneFicheCinemaType->handleRequest($request);

        if ($listeDesFichiersVideoNonLierAUneFicheCinemaType->isSubmitted() && $listeDesFichiersVideoNonLierAUneFicheCinemaType->isValid()) {
            // Récupère l'entité
            $fichierVideoEntity = $listeDesFichiersVideoNonLierAUneFicheCinemaType->get('nom')->getData();
            $arrayCollectionFilmEntity = [];
            $films = $filmRepository->findBy([], ['titre' => 'ASC']);
            foreach ($films as $key => $entity) {
                // 
                $titreDuFilm = strtolower($entity->getTitre());
                $nomDuFichierVideo = strtolower($fichierVideoEntity->getNom());
                $search = str_contains($nomDuFichierVideo, $titreDuFilm);
                if ($search) {
                    $arrayCollectionFilmEntity[] = $entity;
                }
            }
            $arrayCollectionFilmEntity = $this->createForm(CollectionFilmType::class, $arrayCollectionFilmEntity);

            return $this->render('videotheque/_detailFile.html.twig', [
                'listeDesFichiersVideoNonLierAUneFicheCinemaType' => $listeDesFichiersVideoNonLierAUneFicheCinemaType,
                'arrayCollectionFilmEntity' => $arrayCollectionFilmEntity,
                // 'ficheCineMatchAvecFichierVideoType' => $ficheCineMatchAvecFichierVideoType,
            ]);

            // Parcourir toutes les fiches et vérifier si le titre du film est contenu dans un ou plusieurs noms du fichier.
            // puis restituer les résultats dans un fieldset avec checkbox
        }

        return $this->render('videotheque/liste-des-fichiers-video.html.twig', [
            'listeDesFichiersVideoNonLierAUneFicheCinemaType' => $listeDesFichiersVideoNonLierAUneFicheCinemaType,
        ]);
    }

    


    /*
    #[Route('/search', name: 'app_scraping_search')]
    public function search(VideoFileRepository $videoFileRepository): Response
    {
        $films = $videoFileRepository->findAll();
        $this->extraireLesDonneesDesUrlsQuiMatchAvecLesTitresDesFilms( $films[0]->getNom());

        // Affichage de la page
        return $this->render('scraping/search.html.twig', [
            'video_files' => $videoFileRepository->findAll(),
        ]);
    }
    */

    /*
    private function extraireLesDonneesDesUrlsQuiMatchAvecLesTitresDesFilms(string $titreDuFilm)
    {
        $formatChaineTitreDuFilm = urlencode($titreDuFilm);
        // $url = 'https://www.allocine.fr/rechercher/movie/?q=' . $formatChaineTitreDuFilm ;
        $url = 'https://www.allocine.fr/rechercher/movie/?q=matrix';

        // EN PHP NATIF -Téléchargeons la page
        $htmlContent = file_get_contents($url);
        // dump($htmlContent);

        // AVEC GOUTTE
        $client = new Client();

        // Téléchargeons la page
        $crawler  = $client->request('GET', $url);
        // dump($crawler ->html());

        // $domDocument = new DOMDocument();
        // $f = $domDocument->loadHTML($crawler->html());
        // dump($f);

        $listDesFiches = $crawler->filter('div.sub-body > main#content-layout section.section.movies-results > ul')->children();
        // dump("Renvoie le nombre d'enfants <li> dans la liste <ul> soit : " . count($listDesFiches) . " fiche(s) cinéma pour ce titre.");

        $h2 = $listDesFiches->filter('h2.meta-title span');
        dump($h2);
        
        echo("Enumération des fiches cinéma <br>");
        foreach ($listDesFiches as $index => $domElement) {
            echo ('Fiches cinéma n° '. $index . '<hr>');
            dump("Arborescence : " . $domElement->getNodePath());
            dump('Nom de la balise : ' . $domElement->nodeName);
            dump('Renvoie le nombre de noeud enfant : ' . $domElement->childNodes->count());
            // dump("Valeur du noeud (contenu visible sur la page web) : <br>" . $domElement->nodeValue . "<br><br>");
            dump("TextContent (contenu textuel visible sur la page web) : <br>" . $domElement->textContent);
            // dump("fisrt " . $domElement->ownerDocument->documentElement->getElementsByTagName('h2'));
            dump("Défini si un attribut existe pour cette balise : " . $domElement->hasAttributes());
            foreach ($domElement->attributes as $attribute) {
                dump('valeur de l\'attribut : ' . $attribute->textContent);
            }
            foreach ($domElement->childNodes as $key => $childNode) {
                dump($childNode);
            }
        }

    }
    */

    /*
    #[Route('/supprimeLesNomsDesAffiches', name: 'app_supprimeLesNomsDesAffiches')]
    public function supprimeLesNomsDesAffiches(ManagerRegistry $doctrine) : Response
    {
        $entityManager = $doctrine->getManager();
        $films = $doctrine->getRepository(Film::class)->findAll();
        foreach ( $films as $key => $film) {
            $film->setAfficheFilename(null);
            $entityManager->persist($film);
            $entityManager->flush();
        }
        return new JsonResponse(['message' =>  'OK']);
    }
    */
}
