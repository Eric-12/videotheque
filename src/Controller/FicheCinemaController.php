<?php

namespace App\Controller;

use DOMDocument;
use App\Entity\Film;
use App\Form\FilmType;
use App\Form\TitreFilmType;
use App\Repository\FilmRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

#[Route('/fiche/cinema')]
class FicheCinemaController extends AbstractController
{
    #[Route('/', name: 'app_fiche_cinema_index', methods: ['GET','POST'])]
    public function index(EntityManagerInterface $em,FilmRepository $filmRepository, Request $request, PaginatorInterface $paginator): Response
    {
        
        $chercheTitreFilmForm = $this->createForm(TitreFilmType::class);
        $chercheTitreFilmForm->handleRequest($request);
        if ($chercheTitreFilmForm->isSubmitted() && $chercheTitreFilmForm->isValid()) {
            $search = $chercheTitreFilmForm->getData();
            $ficheCine = $filmRepository->findByTitle($search['filterTitle']);
        } else {
            if($request->query->get('sort') !== null && $request->query->get('direction')!== null)
            {
                $ficheCine = $filmRepository->findBy([],[$request->query->get('sort') => $request->query->get('direction')]);
            } else {
                $ficheCine = $filmRepository->findBy([],['titre' => 'ASC']);
            }
           
        }

         // CONFIGURER ET PERSONALISER POUR LE PAGINATEUR
         $pagination = $paginator->paginate(
            $ficheCine,
            $request->query->getInt('page', 1),
            100
        );
        $pagination->setCustomParameters([
            'align'   => 'center', // 'left', 'center', or 'right'
            'size'    => 'medium',  // 'small', 'medium', or 'large'
            'rounded' => true,
        ]);
        

        return $this->render('fiche_cinema/index.html.twig', [
            'pagination' => $pagination,
            'chercheTitreFilmForm' => $chercheTitreFilmForm,
        ]);
    }

    
    #[Route('/new', name: 'app_fiche_cinema_new', methods: ['GET', 'POST'])]
    public function new(Request $request, FilmRepository $filmRepository): Response
    {
        $film = new Film();
        $form = $this->createForm(FilmType::class, $film);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $filmRepository->save($film, true);

            return $this->redirectToRoute('app_fiche_cinema_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('fiche_cinema/new.html.twig', [
            'film' => $film,
            'form' => $form,
        ]);
    }


    #[Route('/{id}', name: 'app_fiche_cinema_show', methods: ['GET'])]
    public function show(Film $film): Response
    {
        return $this->render('fiche_cinema/show.html.twig', [
            'film' => $film,
        ]);
    }


    #[Route('/{id}/edit', name: 'app_fiche_cinema_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Film $film, FilmRepository $filmRepository): Response
    {
        $form = $this->createForm(FilmType::class, $film);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $filmRepository->save($film, true);

            return $this->redirectToRoute('app_fiche_cinema_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('fiche_cinema/edit.html.twig', [
            'film' => $film,
            'form' => $form,
        ]);
    }


    #[Route('/{id}', name: 'app_fiche_cinema_delete', methods: ['POST'])]
    public function delete(Request $request, Film $film, FilmRepository $filmRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$film->getId(), $request->request->get('_token'))) {
            $filmRepository->remove($film, true);
        }

        return $this->redirectToRoute('app_fiche_cinema_index', [], Response::HTTP_SEE_OTHER);
    }
}
