<?php

namespace App\Controller;

use App\Entity\Realisateur;
use App\Form\RealisateurType;
use App\Repository\RealisateurRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

#[Route('/realisateur')]
class RealisateurController extends AbstractController
{
    #[Route('/', name: 'app_realisateur_index', methods: ['GET'])]
    public function index(RealisateurRepository $realisateurRepository, PaginatorInterface $paginator, Request $request): Response
    {
        // CONFIGURER ET PERSONALISER POUR LE PAGINATEUR
        $pagination = $paginator->paginate(
            $realisateurRepository->findBy([],['nom' => 'ASC']),
            $request->query->getInt('page', 1),
            25
        );
        $pagination->setCustomParameters([
            'align'   => 'center', // 'left', 'center', or 'right'
            'size'    => 'medium',  // 'small', 'medium', or 'large'
            'rounded' => true,
        ]);

        return $this->render('realisateur/index.html.twig', [
            'pagination' => $pagination,
            'realisateurs' => $realisateurRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_realisateur_new', methods: ['GET', 'POST'])]
    public function new(Request $request, RealisateurRepository $realisateurRepository): Response
    {
        $realisateur = new Realisateur();
        $form = $this->createForm(RealisateurType::class, $realisateur);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $realisateurRepository->save($realisateur, true);

            return $this->redirectToRoute('app_realisateur_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('realisateur/new.html.twig', [
            'realisateur' => $realisateur,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_realisateur_show', methods: ['GET'])]
    public function show(Realisateur $realisateur): Response
    {
        return $this->render('realisateur/show.html.twig', [
            'realisateur' => $realisateur,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_realisateur_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Realisateur $realisateur, RealisateurRepository $realisateurRepository): Response
    {
        $form = $this->createForm(RealisateurType::class, $realisateur);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $realisateurRepository->save($realisateur, true);

            return $this->redirectToRoute('app_realisateur_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('realisateur/edit.html.twig', [
            'realisateur' => $realisateur,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_realisateur_delete', methods: ['POST'])]
    public function delete(Request $request, Realisateur $realisateur, RealisateurRepository $realisateurRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$realisateur->getId(), $request->request->get('_token'))) {
            $realisateurRepository->remove($realisateur, true);
        }

        return $this->redirectToRoute('app_realisateur_index', [], Response::HTTP_SEE_OTHER);
    }
}
