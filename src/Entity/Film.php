<?php

namespace App\Entity;

use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use App\Repository\FilmRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;


#[ORM\Entity(repositoryClass: FilmRepository::class)]
#[UniqueEntity('titre')]
class Film
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $url = null;

    #[ORM\Column(length: 255, unique: true)]
    private ?string $titre = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $titreOriginal = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $dateSortie = null;
    
    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $date = null;

    #[ORM\Column(nullable: true)]
    private ?int $duree = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $synopsis = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $linkUrl = null;

    #[ORM\Column(type: Types::DECIMAL, precision: 4, scale: 1, nullable: true)]
    private ?string $score = null;

    #[ORM\Column(nullable: true)]
    private ?int $numPage = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $afficheFilename = null;

    #[ORM\ManyToMany(targetEntity: Acteur::class, inversedBy: 'films')]
    private Collection $acteur;

    #[ORM\ManyToMany(targetEntity: Genre::class, inversedBy: 'films', cascade:["persist"])]
    private Collection $genre;

    #[ORM\ManyToMany(targetEntity: Realisateur::class, inversedBy: 'films')]
    private Collection $realisateur;

    #[ORM\OneToMany(mappedBy: 'film', targetEntity: VideoFile::class)]
    private Collection $videoFile;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $infoDateDureeGenre = null;


    public function __construct()
    {
        $this->acteur = new ArrayCollection();
        $this->genre = new ArrayCollection();
        $this->realisateur = new ArrayCollection();
        $this->videoFile = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setTitre(string $titre): self
    {
        $this->titre = $titre;

        return $this;
    }

    public function getTitreOriginal(): ?string
    {
        return $this->titreOriginal;
    }

    public function setTitreOriginal(?string $titreOriginal): self
    {
        $this->titreOriginal = $titreOriginal;

        return $this;
    }

    public function getDateSortie(): ?string
    {
        return $this->dateSortie;
    }

    public function setDateSortie(?string $dateSortie): self
    {
        $this->dateSortie = $dateSortie;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(?\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getDuree(): ?int
    {
        return $this->duree;
    }

    public function setDuree(?int $duree): self
    {
        $this->duree = $duree;

        return $this;
    }

    public function getSynopsis(): ?string
    {
        return $this->synopsis;
    }

    public function setSynopsis(?string $synopsis): self
    {
        $this->synopsis = $synopsis;

        return $this;
    }

    public function getLinkUrl(): ?string
    {
        return $this->linkUrl;
    }

    public function setLinkUrl(?string $linkUrl): self
    {
        $this->linkUrl = $linkUrl;

        return $this;
    }

    public function getScore(): ?string
    {
        return $this->score;
    }

    public function setScore(?string $score): self
    {
        $this->score = $score;

        return $this;
    }

    public function getNumPage(): ?int
    {
        return $this->numPage;
    }

    public function setNumPage(?int $numPage): self
    {
        $this->numPage = $numPage;

        return $this;
    }

    public function getAfficheFilename(): ?string
    {
        return $this->afficheFilename;
    }

    public function setAfficheFilename(?string $afficheFilename): self
    {
        $this->afficheFilename = $afficheFilename;

        return $this;
    }

    /**
     * @return Collection<int, Acteur>
     */
    public function getActeur(): Collection
    {
        return $this->acteur;
    }

    public function addActeur(Acteur $acteur): self
    {
        if (!$this->acteur->contains($acteur)) {
            $this->acteur->add($acteur);
        }

        return $this;
    }

    public function removeActeur(Acteur $acteur): self
    {
        $this->acteur->removeElement($acteur);

        return $this;
    }

    /**
     * @return Collection<int, Genre>
     */
    public function getGenre(): Collection
    {
        return $this->genre;
    }

    public function addGenre(Genre $genre): self
    {
        if (!$this->genre->contains($genre)) {
            $this->genre->add($genre);
        }

        return $this;
    }

    public function removeGenre(Genre $genre): self
    {
        $this->genre->removeElement($genre);

        return $this;
    }

    /**
     * @return Collection<int, Realisateur>
     */
    public function getRealisateur(): Collection
    {
        return $this->realisateur;
    }

    public function addRealisateur(Realisateur $realisateur): self
    {
        if (!$this->realisateur->contains($realisateur)) {
            $this->realisateur->add($realisateur);
        }

        return $this;
    }

    public function removeRealisateur(Realisateur $realisateur): self
    {
        $this->realisateur->removeElement($realisateur);

        return $this;
    }

    /**
     * @return Collection<int, VideoFile>
     */
    public function getVideoFile(): Collection
    {
        return $this->videoFile;
    }

    public function addVideoPath(VideoFile $videoFile): self
    {
        if (!$this->videoFile->contains($videoFile)) {
            $this->videoFile->add($videoFile);
            $videoFile->setFilm($this);
        }

        return $this;
    }

    public function removeVideoFile(VideoFile $videoFile): self
    {
        if ($this->videoFile->removeElement($videoFile)) {
            // set the owning side to null (unless already changed)
            if ($videoFile->getFilm() === $this) {
                $videoFile->setFilm(null);
            }
        }

        return $this;
    }

    public function getInfoDateDureeGenre(): ?string
    {
        return $this->infoDateDureeGenre;
    }

    public function setInfoDateDureeGenre(?string $infoDateDureeGenre): self
    {
        $this->infoDateDureeGenre = $infoDateDureeGenre;

        return $this;
    }

}
