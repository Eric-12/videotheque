<?php

// src/Twig/AppExtension.php
namespace App\Twig;

use PhpParser\Node\Expr\Cast\String_;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class AppExtension extends AbstractExtension
{
    public function getFilters()
    {
        return [
            new TwigFilter('getExtension', [$this, 'getExtension']),
        ];
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('is_streamable', [$this, 'is_streamable']),
            new TwigFunction('getFilename', [$this, 'getFilename']),
        ];
    }


    public function getExtension($path): String
    {
        $splitPath = explode('.', $path);
        return end($splitPath);
    }

    public function getFilename($path): String
    {
        $splitPath = explode('/', $path);
        return end($splitPath);
    }

    public function is_streamable($path): String
    {
        $splitPath = explode('.', $path);
        $fileExtention = end($splitPath);
        $formatVideoStremable =['mp4', 'webm', 'm4v'];

        return in_array($fileExtention, $formatVideoStremable);
    }

}