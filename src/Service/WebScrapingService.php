<?php

namespace App\Service;

use Goutte\Client;
use App\Entity\Film;
use App\Entity\Genre;
use App\Entity\Acteur;
use App\Entity\Realisateur;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class WebScrapingService
{

    public function extraireFicheCineDePageWeb(int $page)
    {
        $url = 'https://www.allocine.fr/film/fichefilm_gen_cfilm=' . $page . '.html';
        $urlCasting = 'https://www.allocine.fr/film/fichefilm-' . $page . '/casting';

        // Créons un nouveau client
        $client = new Client(HttpClient::create(['timeout' => 60]));

        /* --------------------------------
                    PAGE PRINCIPAL
        --------------------------------- */
        // Téléchargeons la page
        $crawler = $client->request('GET', $url);

        // Vérifions que l'url demandée ne renvoi pas une page d'erreur
        $error404 =  $crawler->filter('div.error-page.error-404');
        if (count($error404) == 0) {
            // TITRE DU FILM
            $filterTitre = $crawler->filter('div.titlebar > div.titlebar-title.titlebar-title-lg');
            $titre = count($filterTitre) > 0 ? trim($filterTitre->text()) : null;
            if ($titre == null) return;

            // RESUME
            $filterSynopsis = $crawler->filter('#synopsis-details > div.content-txt');
            $synopsis = count($filterSynopsis) > 0 ? trim($filterSynopsis->text()) : null;
            if ($synopsis == null) return;

            // TITRE ORIGINAL DU FILM
            $filterTitreOriginal = $crawler->filter('div.meta > div.meta-body > div.meta-body-item:last-child');
            if (count($filterTitreOriginal) > 0) {
                if (strpos($filterTitreOriginal->text(), 'Titre original') !== FALSE) {
                    $titreOriginal = trim(str_replace('Titre original', '', $filterTitreOriginal->text()));
                } else {
                    $titreOriginal = null;
                }
            }

            // LIEN DE L'AFFICHE
            $filterImg = $crawler->filter('figure.thumbnail img.thumbnail-img');
            $urlImg = count($filterImg) > 0 ? $filterImg->attr('src') : '...';

            // INFORMATIONS DU FILM
            $filterInfoFilm = $crawler->filter('div.meta > div.meta-body > div.meta-body-item.meta-body-info');
            $infoDateDureeGenre = $filterInfoFilm->text();

            // DATE DE SORTIE
            $filterDateSortie = $filterInfoFilm->filter('.date');
            $date = count($filterDateSortie) > 0 ? $this->stringDateToDatetime(trim($filterDateSortie->text())) : null;

            // DUREE + GENRE
            if (count($filterInfoFilm) > 0) {
                $infoFilm = explode("/", $filterInfoFilm->text());

                // DUREE
                $duree = count($infoFilm) >= 2 ? $this->stringDureeToInt(trim($infoFilm[1])) : null;

                // GENRE
                if (count($infoFilm) >= 3) {
                    $genre = [];
                    $genre = explode(",", trim($infoFilm[2]));
                } else {
                    $genre = [];
                }
            }

            // NOTES SPECTATEURS
            $filterNote = $crawler->filter('.stareval-note');
            if (count($filterNote) > 0) {
                $score = trim($filterNote->text());
                $score = str_replace(',', '.', $score);
                $note = floatval($score);
            } else {
                $note = null;
            }

            /* --------------------------------
                    PAGE CASTING
            --------------------------------- */
            // Téléchargeons la page d’accueil du site visé
            $crawler = $client->request('GET', $urlCasting);

            // Récupérer le(s) réalisateur(s)
            $realisateur = [];
            $listRealisateur = $crawler->filter('section.casting-director div.person-card.person-card > div.meta > div.meta-title > a.meta-title-link');
            foreach ($listRealisateur as $node) {
                array_push($realisateur, $node->textContent);
            }

            // Récupérer le(s) acteur(s)
            $acteur = [];
            $listActeur = $crawler->filter('section.casting-actor div.person-card.person-card > div.meta > div.meta-title > a.meta-title-link');
            foreach ($listActeur as $node) {
                array_push($acteur, $node->textContent);
            }

            return [
                'numPageWeb' => $page,
                'url' => $url,
                'titre' => $titre,
                'titre_original' => $titreOriginal,
                'url_img' => $urlImg,
                'info_date_duree_genre' =>  $infoDateDureeGenre,
                'date' => $date,
                'duree' => $duree,
                'genre' => $genre,
                'note_spectateurs' => $note,
                'realisateur' => $realisateur,
                'acteur' => $acteur,
                'synopsis' => $synopsis,
            ];
        }
        return;
    }


    public function saveDatafromWebPageToDatabase(array $extractedDataWebPage, ManagerRegistry $doctrine, FileUploader $fileUploader, ValidatorInterface $validator)
    {
        $entityManager = $doctrine->getManager();
        $film = new Film();
        $film->setTitre($extractedDataWebPage['titre']);

        // Validator (le titre de la fiche cinéma doit être unique)
        $errors = $validator->validate($film);
        if (count($errors) > 0) {
            return false;
        }
        
        $film->setTitreOriginal($extractedDataWebPage['titre_original']);
        $film->setNumPage($extractedDataWebPage['numPageWeb']);
        $film->setUrl($extractedDataWebPage['url']);

        $film->setLinkUrl($extractedDataWebPage['url_img']);
        // UPLOAD L'AFFICHE + SAUVEGARDE DU NOM DU FICHIER DANS LA DATABASE
        if ($extractedDataWebPage['url_img'] !== "https://fr.web.img6.acsta.net/c_310_420/commons/v9/common/empty/empty_portrait.png") {
            $newFileName = $fileUploader->uploadByUrl($extractedDataWebPage['titre'], $extractedDataWebPage['url_img']);
            // sauvagarde dans la DB le nom de l'affiche téléchargé
            $film->setAfficheFilename($newFileName);
        }

        $film->setInfoDateDureeGenre($extractedDataWebPage['info_date_duree_genre']);
        $film->setDate($extractedDataWebPage['date']);
        $film->setDuree($extractedDataWebPage['duree']);
        $film->setScore($extractedDataWebPage['note_spectateurs']);
        $film->setSynopsis($extractedDataWebPage['synopsis']);
        foreach ($extractedDataWebPage['realisateur'] as $entity) {
            $realisateur = $doctrine->getRepository(Realisateur::class)->findOneBy(['nom' => trim($entity)]);
            if (is_null($realisateur)) {
                $realisateur = new Realisateur();
                $realisateur->setNom(trim($entity));
            }
            $realisateur->addFilm($film);
            $entityManager->persist($realisateur);
        }
        /*  AUTRE METHODE POUR AJOUTER UNE COLLECTION DE REALISATEUR 
            CONSISTE A CREER UN OBJET REALISATEUR
            PUIS DE L'AJOUTER VIA L'ENTITE FILM 
            POUR CELA ON DOIT AUTORISER LA PERSISTANCE DE DONNEE EN CASCADE
            DANS L'ENTITE FILM ON AJOUTE cascade={"persist"} ou pour PHP > 8.0 ["persist"])
                #[ORM\ManyToMany(targetEntity: Realisateur::class, inversedBy: 'films', cascade : ["persist"])]

        foreach ($extractedDataWebPage['realisateur'] as $entity) {
            $realisateur = $doctrine->getRepository(Realisateur::class)->findOneBy(['nom' => trim($entity)]);
            if(is_null($realisateur)) {
                $realisateur = new Realisateur();
                $realisateur->setNom(trim($entity));
            }
            $film->getRealisateur()->add($realisateur);
        */
        
        foreach ($extractedDataWebPage['acteur'] as $entity) {
            $acteur = $doctrine->getRepository(Acteur::class)->findOneBy(['nom' => trim($entity)]);
            if (is_null($acteur)) {
                $acteur = new Acteur();
                $acteur->setNom(trim($entity));
            }
            $acteur->addFilm($film);
            $entityManager->persist($acteur);
        }

        foreach ($extractedDataWebPage['genre'] as $entity) {
            $genre = $doctrine->getRepository(Genre::class)->findOneBy(['nom' => trim($entity)]);
            if (is_null($genre)) {
                $genre = new Genre();
                $genre->setNom(trim($entity));  
            }
            $genre->addFilm($film);
            $entityManager->persist($genre);
        }
        // tell Doctrine you want to (eventually) save the Product (no queries yet)
        $entityManager->persist($film);
        // actually executes the queries
        $entityManager->flush();
        return true;
    }


    private function stringDateToDatetime(string $date)
    {
        $arrayDate = explode(' ', $date);
        if (count($arrayDate) == 3) {
            switch ($arrayDate[1]) {
                case 'janvier':
                    $arrayDate[1] = '1';
                    break;
                case 'février':
                    $arrayDate[1] = '2';
                    # code...
                    break;
                case 'mars':
                    $arrayDate[1] = '3';
                    # code...
                    break;
                case 'avril':
                    $arrayDate[1] = '4';
                    # code...
                    break;
                case 'mai':
                    $arrayDate[1] = '5';
                    # code...
                    break;
                case 'juin':
                    $arrayDate[1] = '6';
                    # code...
                    break;
                case 'juillet':
                    $arrayDate[1] = '7';
                    # code...
                    break;
                case 'août':
                    $arrayDate[1] = '8';
                    # code...
                    break;
                case 'septembre':
                    $arrayDate[1] = '9';
                    # code...
                    break;
                case 'octobre':
                    $arrayDate[1] = '10';
                    # code...
                    break;
                case 'novembre':
                    $arrayDate[1] = '11';
                    # code...
                    break;
                case 'décembre':
                    $arrayDate[1] = '12';
                    # code...
                    break;
            }
            $newDate = implode('-', $arrayDate);
            return date_create($newDate);
        }
        return null;
    }


    private function stringDureeToInt(string $stringDuree)
    {
        if ($stringDuree !== null) {
            $mn = strpos($stringDuree, 'min');
            $h = strpos($stringDuree, 'h ');
            if ($mn && $h) {
                $time = explode('h', $stringDuree);
                $heureToMin = $time[0] * 60;
                $min = str_replace('min', '', trim($time[1]));
                return (int)$heureToMin + (int)$min;
            }
            return null;
        }
    }
    
}
