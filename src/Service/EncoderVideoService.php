<?php

/*
 * Service d'encodage FFmpeg.
 *
 * (c) Lanza Eric <lanzae32@gmail.com>
 *
 */

namespace App\Service;

use App\Entity\VideoFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;
use Symfony\Component\Validator\Constraints\Collection;

use function PHPSTORM_META\map;

/**
 * Service d'encodage FFmpeg.
 *
 * @author Lanza Eric <lanzae32@gmail.com>
 *
 */
class EncoderVideoService 
{
    private $slugger;
    private $targetFFmpegLogDirectory;
    private $targetUploadVideoDirectory;


    public function __construct($targetFFmpegLogDirectory, $targetUploadVideoDirectory, SluggerInterface $slugger)
    {
        $this->targetFFmpegLogDirectory = $targetFFmpegLogDirectory;
        $this->targetUploadVideoDirectory = $targetUploadVideoDirectory;
        $this->slugger = $slugger;
    }


    public function infoVideo(VideoFile $videoFile) 
    {
        $commands = [
            "C:/ffmpeg/bin/ffmpeg.exe",
            "-i",
            "\"".$videoFile->getRoot()."/".$videoFile->getPath()."\"",
        ];

        $output = shell_exec(implode(' ', $commands)." 1> process/getInfo.log 2>&1");

        $filelog = file_get_contents("process/getInfo.log");

        $streams = explode('Stream #', $filelog);

        array_shift($streams);

        foreach ($streams as $stream) {
            $arrayStream = explode(':', $stream);
            $pos = strpos($arrayStream[2],' ');
            return implode(':',[$arrayStream[0],$arrayStream[0]]);
        }


        try{
            unlink("process/getInfo.log");
        } catch(\Exception $e){
            print_r($e);
        }

        return $streams;

        // $process = new Process($commands);
        // $process->run();

        // $f = $process->getErrorOutput();
        // $d = $process->getOutput();
        // $streams = explode('Stream #', $process->getOutput());
        // $duree = explode(',', $streams[0]);
        // $duree = str_replace('Duration:','', $duree[0]);
        // array_shift($streams);
        // return [$duree, $streams];
    }

    /**
     * @param   \App\Entity\VideoFile   $videoFile  Représente l'entité vidéoFile et donne accès aux valeurs des propriétés via les méthodes getNom(), getTaille(), getRoot(), getPath()
     * @param   mixed                   $data       Représente les valeurs des options d'encodages sousmises lors de la validation du formulaire. 
     * @return  string                              Retourne le contenu de la console sous la forme d'une chaine de caractère.  
     * PROCESS
     * 
     * - La méthode start() pour démarrer un processus de manière asynchrone.
     * $process->start();
     * - La méthode isRunning() pour vérifier si le processus est terminé 
     *     ex: while ($process->isRunning()) { waiting for process to finish }
     * - La méthode wait() permet d'attendre la fin d'un processus lancé de manière asynchrone c'est une méthode bloquante
     *     ex: $process->wait();
     * - La méthode getOutput() pour obtenir la sortie 
     *     $process->getOutput();
     * - Si une Response est envoyé avant qu'un processus enfant n'ait pu se terminer, le processus serveur sera tué. Cela signifie que votre tâche sera arrêtée immédiatement. 
     * - La méthode getErrorOutput() renvoie le contenu de l'erreur sortir. 
     * - Les méthodes getIncrementalOutput() et getIncrementalErrorOutput() renvoient la nouvelle sortie depuis le dernier appel. 
     * - La méthode clearOutput() efface le contenu de la sortie et clearErrorOutput() efface le contenu de la sortie d'erreur. 
     *             
     */
    public function encodeToMp4(VideoFile $videoFile, $data)
    {
        $commands = [
            // Programme d'encodage a exécuter
            "C:/ffmpeg/bin/ffmpeg.exe",
            // Ecrase le fichier de sortie s'il existe
            "-y",
            // Permet de sauvegarder toute les secondes les informations de progression dans un fichier.
            // "-progress",
            // $this->getTargetFFmpegLogDirectory()."/ffmpeg.log",
            // Nom du fichier a convertir
            "-i",
            $videoFile->getRoot() . "/" . $videoFile->getPath(),
        ];

        switch ($data['mutliplexage']) {
            case '0': // Encoder les pistes audio et vidéo
                if ($data['tauxDeCompressionVideo'] !== '-1') {
                    $commands[] = "-crf";
                    $commands[] = $data['tauxDeCompressionVideo'];
                }
                if ($data['ratio'] !== '-1') {
                    $commands[] = "-aspect";
                    $commands[] = $data['ratio'];
                }
                break;
            
            case '1': // Encode la piste vidéo et conserver la piste audio identique à la source
                $commands[] = "-r";
                $commands[] = "29.97";
                $commands[] = "-crf";
                $commands[] = $data['tauxDeCompressionVideo'];
                $commands[] = "-acodec";
                $commands[] = "copy";
                break;

            case '2': // Conserver la piste vidéo identique à la source et encode la piste audio
                $commands[] = "-vcodec";
                $commands[] = "copy";
                break;

            case '3': // Ne pas encoder, assemble les pistes audio/vidéo (demultiplexer / multiplexer).
                $commands[] = "-codec";
                $commands[] = "copy";
                break;
        }

        // Répertoire de sortie du fichier slugyfié
        $commands[] =  $this->getTargetUploadVideoDirectory().'/'. $this->slugger->slug($videoFile->getNom()).$data['formatVideo'];

        /**  
         * Exécute l'encodage avec FFmpeg.exe via le Shell et retourne le résultat dans un fichier (log)
         * 
         * Chaque processus a 3 flux:
         *  - un flux d’entré: STDIN(0)
         *  un flux de sortie standard: STDOUT(1)
         *  un flux de sortie d’erreur: STDERR(2) 
         *  1> process/ffmpeg.log : redirige STDOUT(1) l'affichage du traitement du processus vers le fichier process/ffmpeg.log
         *  2>&1 : redirige STDERR(2) vers STDOUT(1) donc vers le fichier process/ffmpeg.log
          */ 
        // $output = shell_exec(implode(' ', $commands)." 1> process/ffmpeg.log 2>&1");
        // return $output;


        $process = new Process($commands);
        // Délai d'expiration du processus 
        $process->setTimeout(3600);
        // Délai d'inactivité du processus 
        // $process->setIdleTimeout(3600);
        // Désactiver la sortie
        // $process->disableOutput();
        $process->run();
        // executes after the command finishes
        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }
        return $process->getErrorOutput(); 

        // $process = new Process($commands);
        // $process->setTimeout(3600);
        // // $process->disableOutput();
        // $process->start();
        // while ($process->isRunning()) {
        //     // waiting for process to finish
        // }
        // // $readFile = file_get_contents($this->getTargetFFmpegLogDirectory()."/ffmpeg.log");
        // // return $readFile; 
        // return $process->getErrorOutput();
    }

    public function getTargetFFmpegLogDirectory()
    {
        return $this->targetFFmpegLogDirectory;
    }

    public function getTargetUploadVideoDirectory()
    {
        return $this->targetUploadVideoDirectory;
    }

    /*  -------------------  FFMPEG  ----------------------
        liste de formats
        ffmpeg -formats

        liste des codecs
        ffmpeg -codecs

                                Streams:         _________      _________      _________
                        #0:0           |         |    |         |    |         |
                        +------->------->| decoder |--->| filters |--->| encoder |--->----+
                        |                |_________|    |_________|    |_________|        |
        __________     |                 _________      _________      _________         |       ___________
        |          |    | #0:1           |         |    |         |    |         |        |      |           |
        | Input #O |----+------->.   ,-->| decoder |--->| filters |--->| encoder |--->----+----->| Output #O |
        |__________|    |         \ /    |_________|    |_________|    |_________|        |      |___________|
                        |          X      _______________________________________         |
                        | #0:2    / \    |                                       |        |
                        +------->´   `-->|                 copy                  |--->----+
                                        |_______________________________________|
                    \________/ \______/                                             \________/
                    demuxer   mapping                                                muxer

        
        -i : suivi du chemin d'acces au fichier video

        -map : permet aussi d'ajouter un flux

        -r : nombre d'images  ex: "-r 29.97"

        -codec : codec audio et vidéo ex: "-codec copy" = identique à la source

        -vcodec : codec vidéo  ex: "-vcodec libx264"
        -c:v : codec video ex: "-c:v libx264"

        -acodec : codec audio ex:"-acodec aac"
        -c:a : codec audio ex: "-c:a aac"

        -scodec : codec sous-titre
        -c:s : codec sous-titre

        -c:v -c:a : ffmpeg -i D:\FILMS\test-format\60-secondes-chrono.avi -c:v h264 -c:a aac D:\FILMS\test-format\60-secondes-chrono-video264.mp4

        -crf : Facteur de taux constant (CRF) -  (0 -> 51) 0 sans perte
            Conserve la meilleure qualité sans ce soucier de la taille du fichier. C'est le mode de contrôle de débit recommandé pour la plupart des utilisations. 
            La plage de l'échelle CRF est de 0 à 51, où 0 est sans perte sinon utilisez -qp 0, 23 est la valeur par défaut et 51 est la pire qualité possible
            -qscale :  0 sans perte
            ffmpeg -i input -c:v libx264 -preset slow -crf 22 -c:a copy output.mkv
            préserver la qualité de votre fichier vidéo source : -qscale 0

        -codec copy ou alors -codec: copy copie tous les flux sans réencodage.(multiplexage) 
            ffmpeg -i D:\FILMS\test-format\The.Equalizer.mkv -codec copy  D:\FILMS\test-format\The.Equalizer.mp4
            ffmpeg -i D:\FILMS\test-format\The.Equalizer.mkv -vcodec copy -acodec aac  D:\FILMS\test-format\The.Equalizer2.mp4

        -b : video bitrate ex: "-b 768k"
        -b:v : video bitrate ex: "-b:v 1600k"

        -ab : audio bitrate (96, 112, 128, 160, 192, 256, 320) ex: "ab 64k"
        -b:a : audio bitrate ex: "-b:a 64k"

        -aspect : ratio (16:9, 4:3) ex: "-aspect 16:9"
        
        -ar : Définit la fréquence audio du fichier de sortie. Les valeurs communes utilisées sont 22050, 44100, 48000 Hz. 

        -ac : Définit le nombre de canaux audio.

        –ab : Indique le débit binaire audio. 

        -f : Format du fichier de sortie  ex: -f mp3. 

        -vn : pas de flux vidéo dans le fichier de sortie.

        -an : pas de flux audio dans le fichier de sortie.

        -t 10 : les 10 premières secondes d’un fichier video.mp4

        -ss 00:00:50 : Indique l’heure de début du clip vidéo. Dans notre exemple, l’heure de début correspond à la 50e seconde.

        -to 00:06:53 : Indique l’heure de fin du clip vidéo.

        Convertir seulement la piste audio au format mp3
            0:0 stream video, 0:1 audio, 0:2 subtitle 
            ffmpeg -i "./3-DAYS-TO-KILL.mp4" -map 0:1 filename.mp3

        Convertir seulement la piste audio au format flac 
            ffmpeg -i "./3-DAYS-TO-KILL.mp4" -map 0:1 -c:a flac filename.mp4

        Extraire les sous-titres d’un fichier MULTISUB
            ffmpeg -i filename.mp4
            ffmpeg -i filename.mp4 -map 0:s:[i] filename.srt
        
        Conserver la piste audio identique à la source et ajouter une pista audio converti au format flac
            ffmpeg -i "./3-DAYS-TO-KILL.mp4" -map 0:1 -map 0:1 -c:a:1 copy -c:a:1 flac filename.mp4

        Conversion sans perte de qualité
            ffmpeg -i D:\FILMS\test-format\60-secondes-chrono.avi -qscale 0 D:\FILMS\test-format\video001.mp4

        Copie le flux vidéo a l'identique, convertit la piste audio n°1 (french) en AAC, copie la piste audio n°1, copie la piste audio n°2 (anglais).
            ffmpeg -i "./3 DAYS TO KILL.mkv" -map 0:0 -c:v:0 copy -map 0:1 -c:a:1 aac -map 0:1 -c:a:1 copy -map 0:2 -c:a:2 copy -map 0:3 -c:s:3 mov_text "./3 DAYS TO KILL.mp4"

        Copie les sous-titres
            ffmpeg -i "3 DAYS TO KILL.mkv" -map 0:3 -c:s copy  "3 DAYS TO KILL.srt"

        Intégrer les sous-titres à partir d'un ficihers video avec 3 pistes audio et 1 fichier de sous-titres *.srt
            ffmpeg -i "./3 DAYS TO KILL.mp4" -i "./3 DAYS TO KILL.srt" -map 0:0 -map 0:1 -map 0:2 -map 0:3 -map 1:0 -c:v copy -c:a copy -scodec mov_text  -metadata:s:s:0 language=French "./3 DAYS TO KILL(2).mp4"

        


        --------------------------------------------------------------------------------
        Audio Codec: 

        Opus libopus 
            extension: .opus
            type mime: audio/ogg

        Vorbis
            extension: .ogg, .oga, .sb0
            type mime: audio/vorbis, application/ogg, audio/vorbis-config, audio/ogg

       --------------------------------------------------------------------------------
        Video Codec:
        
        VP8 - VP9
        C'est un format de compression vidéo ouvert appartenant à Google remplaçant éventuellement le H.264, contenu dans les formats WebM 
        il peut donc être lu par les navigateurs prenant en charge ce format (Firefox, Google Chrome, Opera...).
            type mime:video/VP9 
        
        AV1
        C'est un codec vidéo ouvert et libre de droits1 créé en 2018 et conçu pour la diffusion de flux vidéo sur Internet et réseaux IP2 comme successeur de VP9. 
        
    */
}