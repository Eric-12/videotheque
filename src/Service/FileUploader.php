<?php

namespace App\Service;

use SplFileInfo;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

class FileUploader
{
    private $targetDirectory;
    private $slugger;

    /* 
    Création d'un service de téléchargement
        - On déclare ce service dans config/services.yaml, on definit l'argument $targetDirectory 
            services:
                App\Service\FileUploader:
                    arguments:
                        $targetDirectory: '%affiche_directory%'
        - La valeur de l'argument $targetDirectory est défini dans config/services.yaml
            parameters:
                affiche_directory: '%kernel.project_dir%/public/uploads/affiches'
    */
    public function __construct($targetDirectory, SluggerInterface $slugger)
    {
        $this->targetDirectory = $targetDirectory;
        $this->slugger = $slugger;
    }

    public function uploadByForm(UploadedFile $file)
    {
        $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
        $safeFilename = $this->slugger->slug($originalFilename);
        $fileName = $safeFilename.'-'.uniqid().'.'.$file->guessExtension();

        try {
            $file->move($this->getTargetDirectory(), $fileName);
        } catch (FileException $e) {
            // ... handle exception if something happens during file upload
        }

        return $fileName;
    }

    public function uploadByUrl(string $titre, string $url)
    {
        $infoFile = new SplFileInfo($url);
        // Récupère l'extension du fichier
        $extention = $infoFile->getExtension();
        // Génère un nouveau nom unique à partir du titre du film pour le fichier téléchargé
        $safeFilename = $this->slugger->slug($titre);
        $fileName = $safeFilename.'-'.uniqid().'.'.$extention;
        // Récupérer le fichier depuis l'url
        $downloadFile = file_get_contents($url);
        // Sauvegarder le fichier
        file_put_contents($this->getTargetDirectory().'/'.$fileName, $downloadFile);

        return $fileName;
    }

    public function getTargetDirectory()
    {
        return $this->targetDirectory;
    }
}