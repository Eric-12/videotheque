<?php

namespace App\Service;

class CleanerPathService {

    public function ListeDesFichiersDuDossier(string $path) 
    {
        $fichiersDuDossier = [];
        if (is_dir($path)) {
            $dir = opendir($path);
            while ($contentDir = readdir($dir)) {
                if ($contentDir != '.' && $contentDir != '..') {
                    $fichiersDuDossier[] = $contentDir;
                }
            }
            closedir($dir);
        }
        return $fichiersDuDossier;
    }

    public function supprimeLesAffiches(string $dossierDeStockageDesAffiches, array $affiches ) 
    {
        foreach ($affiches as $affiche) {
            try {
                unlink($dossierDeStockageDesAffiches . "/" . $affiche);
            } catch (\Throwable $th) {
                // throw new \Exception('Invalid strength passed ');
            }
            
        }
        return;
    }
}