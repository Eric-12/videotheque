<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\ChoiceList\ChoiceList;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class EncodageffmpegType extends AbstractType
{
    /**
     * @property formatVideo    $formatVideo   Choisir le conteneur (format vidéo) pour votre fichier multimédia.
     * 
     * @property multiplexage   $mutliplexage  Méthode d'écriture du fichier.
     * @property tauxDeCompressionVideo   $tauxDeCompressionVideo  Qualité de la vidéo en sortie.
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add("test", ChoiceType::class, [
                "label" => "Choisir le format conteneur pour votre fichier multimédia.",
                "label_attr" => ['class' => 'fw-semibold'],
                'choices'  => $options["data"],
                'choice_label' => function ($choice, $key, $value) {
                    return $value;
                },
                // 'choice_value' =>  array_keys($options['data']),
                // 'choice_value' => function ($value) {
                //     // return $options['data'];
                // },
                'multiple' => true,
                'expanded' => true,
            ])
            ->add('formatVideo', ChoiceType::class, [
                "label" => "Choisir le format conteneur pour votre fichier multimédia.",
                "label_attr" => ['class' => 'fw-semibold'],
                'choices'  => [
                    'MP4 (codec vidéo: H.264 - codec audio: AAC)' => '.mp4',
                    'WEBM (codec vidéo: VP9 - codec audio: OPUS)' => '.webm'
                ],
                'choice_attr' => function ($choice, $key, $value) {
                    if($value == '.mp4') return ['class' =>'fw-semibold text-success', 'selected' => ''];
                    return [];
                },
                'attr' => ['size' => 2]
            ])
            ->add('mutliplexage', ChoiceType::class, [
                'label' => 'Méthode d\'écriture du fichier',
                "label_attr" => ['class' => 'fw-semibold'],
                'choices'  => [
                    'Encoder les pistes audio et vidéo.'                                                => '0',
                    'Encoder la piste vidéo et conserver la piste audio identique à la source.'         => '1',
                    'Conserver la piste vidéo identique à la source et encoder la piste audio.'         => '2',
                    'Ne pas encoder, modifier le format conteneur (demultiplexer -> multiplexer).'      => '3'
                ],
                'choice_attr' => function ($choice, $key, $value) {
                    if($value == '0') return ['class' =>'fw-semibold text-success', 'selected' => ''];
                    return [];
                },
                'attr' => ['size' => 4],
            ])
            // ->add('codecVideo', ChoiceType::class, [
            //     'label' => 'Codec vidéo: c\'est le programme qui permet de compresser ou décompresser le flux vidéo',
            //     'choices'  => [
            //         'Automatique' => '',
            //         'H 264' => '-vcodec libx264',
            //         'H 265' => '-vcodec libx265',
            //     ],
            //     'choice_attr' => function ($choice, $key, $value) {
            //         if($value == '-vcodec libx264') return ['class' =>'fw-semibold text-success', 'selected' => ''];
            //         return [];
            //     },
            //     'attr' => ['size' => 3]
            // ])
            ->add('tauxDeCompressionVideo', ChoiceType::class, [
                'label' => 'Qualité de la vidéo en sortie',
                "label_attr" => ['class' => 'fw-semibold'],
                'choices'  => [
                    // 'Laisser FFmpeg gérer la qualité'   => '-1',
                    'Sans perte'                        => '0',
                    'Bonne qualité'                     => '10',
                    'Qualité moyenne'                   => '23',
                    'Mauvaise qualité'                  => '37',
                    'Très mauvaise qualité'             => '51',
                ],
                'choice_attr' => function ($choice, $key, $value) {
                    if($value == '23') return ['class' =>'fw-semibold text-success', 'selected' => ''];
                    return [];
                },
                // 'attr' => ['size' => 6]
            ])
            // ->add('framerate', ChoiceType::class, [
            //     'label' => 'Nombre d\'image par seconde',
            //     'choices'  => [
            //         '5'      => '5',
            //         '10'     => '10',
            //         '12'     => '12',
            //         '15'     => '15',
            //         '20'     => '20',
            //         '23.975' => '23.975',
            //         '24'     => '24',
            //         '25'     => '25',
            //         '29.97'  => '29.97',
            //         '30'     => '30',
            //         '48'     => '48',
            //         '50'     => '50',
            //         '59.94'  => '59.94',
            //         '60'     => '60',
            //         '72'     => '72',
            //         '75'     => '75',
            //         '90'     => '90',
            //         '100'    => '100',
            //         '120'    => '120',
            //     ],
            //     'choice_attr' => function ($choice, $key, $value) {
            //         if($value == '30') return ['class' =>'fw-semibold text-success', 'selected' => ''];
            //         return [];
            //     },
            //     'attr' => ['size' => 5]
            // ])
            // ->add('codecAudio', ChoiceType::class, [
            //     'label' => false,
            //     'choices'  => [
            //         'Audio' => [
            //             'Automatique' => '',
            //             'Format AAC (valeur par défaut) - Advanced Audio Coding'                                    => '-acodec aac',
            //             'Format AC-3 (Dolby Digital) - rendu sonore sur 6 canaux'                                   => '-acodec ac3',
            //             'Format Enhanced AC-3 (Dolby Digital) - rendu sonore sur 6 canaux'                          => '-acodec eac3',
            //             'Format Dolby TrueHD - succède à l\'AC-3 - rendu sonore sans perte jusqu\'à 8 canaux audio' => '-acodec eac3',
            //             'Format FLAC'                                                                               => '-acodec flac',
            //         ]
                    
            //     ],
            //     'choice_attr' => function ($choice, $key, $value) {
            //         if($value == '-acodec aac') return ['class' =>'fw-semibold text-success', 'selected' => ''];
            //         return [];
            //     },
            //     'attr' => ['size' => 5]
            // ]) 
            // ->add('tauxDeCompressionAudio', ChoiceType::class, [
            //     'label' => 'Taux de compression audio', 
            //     'choices'  => [
            //         'Automatique' => '',
            //         '96kbps' => '96',
            //         '112kbps' => '112',
            //         '128kbps (valeur par defaut)' => '128',
            //         '160kbps' => '160',
            //         '192kbps' => '192',
            //         '256kbps' => '256',
            //         '320kbps' => '320'   
            //     ],
            //     'choice_attr' => function ($choice, $key, $value) {
            //         if($value == '128')  return ['class' =>'fw-semibold text-success', 'selected' => ''];
            //         return [];
            //     },
            //     'attr' => ['size' => 5]
            // ]) 
            ->add('ratio', ChoiceType::class, [
                'label' => 'Ratio : c\'est le rapport entre la largeur et la hauteur de votre vidéo en pixels',
                "label_attr" => ['class' => 'fw-semibold'],
                'choices'  => [
                    'Identique à la source' => '-1',
                    'Format cinéma (16/9)' => '16:9',
                    'Format TV (4/3)' => '4:3' ,
                    '16:10' => '16:10',
                    '5:4' => '5:4',
                    '2:21:1' => '2:21:1',
                    '2:35:1' => '2:35:1',
                    '2:39:1' => '2:39:1'
                ],
                'choice_attr' => function ($choice, $key, $value) {
                    if($value == '-1') return ['class' =>'fw-semibold text-success', 'selected' => ''];
                    return [];
                },
                // 'attr' => ['size' => 4]
            ])
            // ->add('imageParSeconde', ChoiceType::class, [
            //     'choices'  => [
            //         'Automatique' => '',
            //         'H 264' => '-vcodec libx264',
            //     ],
            // ])
            // ->add('formatDeSortie', ChoiceType::class, [
            //     'choices'  => [
            //         'Automatique' => '',
            //         'H 264' => '-vcodec libx264',
            //         'No' => false,
            //     ],
            // ])
            // ->add('save', SubmitType::class, [
            //     'label' => "Démarrer l'encodage"
            // ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
