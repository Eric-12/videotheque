<?php

namespace App\Form;

use App\Entity\Film;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class CollectionFilmType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('path', ChoiceType::class, [
                'label' => false,
                'choices' => $options['data'],
                'choice_label' => function ($choice, $key, $value) {
                    return $choice->getTitre() . " - " . $choice->getSynopsis() ;
                },
                'expanded' => true,
            ])
            ->add('save', SubmitType::class, [
                'row_attr' => ['class' => 'd-flex flex-column mt-4'],
                'attr' => ['class' => 'btn btn-outline-light'],
                'label' => 'Sauvegarder'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
