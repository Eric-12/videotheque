<?php

namespace App\Form;

use App\Entity\Film;
use App\Form\GenreType;
use App\Form\ActeurType;
use App\Form\RealisateurType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class FilmType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('fichierAffiche', FileType::class, [
                'label' => 'Affiche (Fichier image: gif, jpg, png...)',
                'mapped' => false,
                'constraints' => [
                    new File([
                        'maxSize' => '1024k',
                        'mimeTypes' => [
                            'image/gif',
                            'image/jpeg',
                            'image/png'
                        ],
                        'mimeTypesMessage' => 'Please upload a valid image document',
                    ])
                ],
                'required' => false
            ])
            ->add('titre')
            ->add('titreOriginal')
            ->add('infoDateDureeGenre', null, ['label'=>'Infos groupées'])
            ->add('date', DateType::class, ['label'=>'Date de sortie'])
            ->add('duree', null, ['label'=>'Durée en minutes'])
            ->add('score', null, ['label'=>'Note des spectateurs'])
            ->add('genre', CollectionType::class, [
                'label' => 'Genre(s)',
                'entry_type' => GenreType::class,
                'entry_options' => [
                    'label' => false,
                ],
                'prototype_options'  => [
                    'help_attr' => ['class' => 'text-light'],
                    'help' => 'Insèrer un message...',
                ],
                // 'prototype' => true,
                'allow_add' => true,
                'disabled' => true,
            ])
            ->add('realisateur', CollectionType::class, [
                'label' => 'Réalisateur(s)',
                'entry_type' => RealisateurType::class,
                'entry_options' => [
                    'label' => false,
                ],
                'allow_add' => true,
                'disabled' => true,
            ])
            ->add('acteur', CollectionType::class, [
                'label' => 'Acteur(s)',
                'entry_type' => ActeurType::class,
                'entry_options' => [
                    'label' => false
                ],
                'allow_add' => true,
                'disabled' => true,
            ])
            ->add('synopsis')
            ->add('url', null, ['label'=>'URL de la page web'])
            ->add('linkUrl', null, ['label'=>'URL de l\'affiche'])
            ->add('numPage', null, ['label'=>'Numero de page'])
            // ->add('save', SubmitType::class, [
            //     'attr' => ['class' => 'btn btn-primary'],
            //     'label' => 'Enregistrer les modifications'
            // ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Film::class,
        ]);
    }
}
