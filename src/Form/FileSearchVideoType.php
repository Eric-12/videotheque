<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class FileSearchVideoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('dir',TextType::class,[
                // 'row_attr' => ['class' => 'mb-3'],
                'label' => 'Saisir le chemin du dossier à parcourir',
                'attr' => [
                    // 'class' => 'form-control',
                    'placeholder' => 'D:\FILMS\HD',
                ],
            ])
            ->add('save', SubmitType::class, [
                'attr' => ['class' => 'btn btn-outline-light'],
                'label' => 'Parcourir...'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
