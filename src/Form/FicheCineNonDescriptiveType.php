<?php

namespace App\Form;

use App\Entity\Film;
use App\Repository\FilmRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class FicheCineNonDescriptiveType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('allSelected',CheckboxType::class,[
                'label' => 'Tout cocher',
                'row_attr' => ['class' =>'d-flex flex-column text-bg-light p-2 mb-3'],
                'required' => false,
            ])
            ->add('filter', EntityType::class, [ 
                'row_attr' => ['class' =>'mb-3'],
                'attr' => ['style' => 'max-height:400px; overflow-y:scroll;'],
                //Sélectionne la table
                'class' => Film::class,
                'label' => false,
                // Affiche les valeurs de champs
                'choice_label' => 'titre',
                // Valeurs renvoyés lors de la sélection de l'item
                'choice_value' => 'id',
                // REQUETE SQL PERSONALISEE
                'query_builder' => function (FilmRepository $er) {
                    return $er->createQueryBuilder('film')
                            ->where('film.synopsis IS NULL')
                            ->andWhere('film.dateSortie IS NULL')
                            ->andWhere('film.afficheFilename IS NULL')
                            ->orderBy('film.titre', 'ASC')
                    ;
                },
                'multiple' => true,
                'expanded' => true,
            ])
            ->add('delete', SubmitType::class,[
                'label' => 'Supprimer',
                'row_attr' => ['class' => 'd-flex flex-column'],
                'attr' => ['class' => 'btn btn-warning'],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}