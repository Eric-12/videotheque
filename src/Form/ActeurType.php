<?php

namespace App\Form;

use App\Entity\Acteur;
use App\Form\FilmType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class ActeurType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('nom', null, [
                // 'label' => false 
            ])
            // ->add('films', CollectionType::class, [
            //     'label' => 'A également joué dans...',
            //     'entry_type' => FicheCineType::class,
            //     'entry_options' => [
            //         'label' => false
            //     ],
            // ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Acteur::class,
        ]);
    }
}
