<?php

namespace App\Form;

use App\Entity\Genre;
use App\Entity\Acteur;
use App\Entity\Realisateur;
use App\Repository\FilmRepository;
use App\Repository\GenreRepository;
use App\Repository\ActeurRepository;
use Symfony\Component\Form\AbstractType;
use App\Repository\RealisateurRepository;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class MultiFilterFilmType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->setMethod('GET')
            ->add('date', HiddenType::class)
            ->add('score', HiddenType::class)
            ->add('duree', HiddenType::class)
            ->add('genre', EntityType::class, [
                /**
                 * Pour la mise en forme du formulaire.
                 * Ajouter form_themes:['bootstrap_5_layout.html.twig'] dans  'config/package/twig'
                 * OU dans le template {% form_theme multiFilterFilmForm 'bootstrap_5_layout.html.twig' %}
                 */
                'label' => 'Genres',
                'label_attr' => ['class' => 'mb-1'],
                // Sélectionne l'entite (table)
                'class' => Genre::class,
                // Spécifie la propriété (nom de colonne) a afficher comme "contenu" dans les champs de la liste déroulante.
                // 'choice_label' => function ($genre) {
                //     return $genre->getNom();
                // },
                'choice_label' => 'nom',
                // Spécifie la propriété (nom de colonne) a afficher dans les attributs "value" des champs de la liste déroulante.
                'choice_value' => 'id',
                // Renvoie une liste déroulante avec des checkBoxs (case à cocher) ou des boutons radios
                'multiple' => true,    // Si multiple=true Active le choix multiple(checkBoxs ou radio) dans la liste déroulante sinon html "select"
                'expanded' => true,   //  Si multiple=true liste de checkBoxs (case à cocher). Si multiple=false affiche sous forme de liste boutons radios.
                // Valeur renvoyé lorsque le champs selectionné est vide. par default : null
                // 'empty_data' => null,
                // REQUETE SQL PERSONALISEE
                'query_builder' => function (GenreRepository $er) {
                    return $er->createQueryBuilder('u')
                    ->orderBy('u.nom', 'ASC');
                },
                // Validation du composant coté front
                'required' => false,
            ])
            ->add('realisateur', EntityType::class, [
                // Mise en forme
                'row_attr' => ['class' => 'mb-3 pt-2 border-top border-secondary'],
                'label' => 'Réalisateurs',
                'label_attr' => ['class' => 'mb-1'],
                'attr'  => [
                    'class' => 'border-0 text-bg-dark fw-semibold', 'size' => '10',
                    'style' => 'max-height:250px; overflow-y:scroll;'
                ],
                // Sélectionne la table
                'class' => Realisateur::class,
                // Affiche les valeurs de champs
                'choice_label' => 'nom',
                // Valeurs renvoyés lors de la sélection de l'item
                // 'choice_value' => 'id',
                 // Renvoie une liste déroulante avec des checkBoxs (case à cocher) ou des boutons radios
                // 'multiple' => true,  // Si multiple=true Active le choix multiple(checkBoxs ou rodio) dans la liste déroulante sinon html "select"
                // 'expanded' => true,   //  Si multiple=true liste de checkBoxs (case à cocher). Si multiple=false affiche sous forme de liste boutons radios.
                'empty_data' => null,
                // REQUETE PERSONALISEE
                'query_builder' => function (RealisateurRepository $er) {
                    return $er->createQueryBuilder('u')
                    ->orderBy('u.nom', 'ASC');
                },
                // Validation du composant coté front
                'required' => false,
            ])
            ->add('acteur', EntityType::class, [
                // Mise en forme
                'row_attr' => ['class' => 'mb-3 pt-2 border-top border-secondary'],
                'label' => 'Acteurs',
                'attr'  => [
                    'class' => 'border-0 text-bg-dark fw-semibold', 'size' => '10',
                    'style' => 'max-height:250px; overflow-y: scroll;'
                ],
                // Sélectionne la table
                'class' => Acteur::class,
                // Affiche les valeurs de champs
                'choice_label' => 'nom',
                // Valeurs renvoyés lors de la sélection de l'item
                // 'choice_value' => 'id',
                'empty_data' => null,
                // Renvoie une liste déroulante avec des checkBoxs (case à cocher) ou des boutons radios
                // 'multiple' => true,    // Si multiple=true Active le choix multiple(checkBoxs ou rodio) dans la liste déroulante sinon html "select"
                // 'expanded' => true,   //  Si multiple=true liste de checkBoxs (case à cocher). Si multiple=false affiche sous forme de liste boutons radios.
                // REQUETE PERSONALISEE
                'query_builder' => function (ActeurRepository $er) {
                    return $er->createQueryBuilder('u')
                    ->orderBy('u.nom', 'ASC');
                },
                // Validation du composant coté front
                'required' => false,
            ])
            ->add('save',SubmitType::class, [
                'row_attr' => ['class' => 'd-flex flex-column mb-3'],
                'label' => 'Rechercher...',
                'attr' => ['class' => 'mt-3 btn btn-outline-light'],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
