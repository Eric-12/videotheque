<?php

namespace App\Form;

use App\Entity\Film;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FicheCineType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('titre', TextType::class, ['label' => false,'disabled' => true])
            // ->add('titreOriginal', null, ['disabled' => true])
            // ->add('synopsis', TextareaType::class, [
            //     'attr' => ['rows' => 5],
            //     'disabled' => true,
            // ])
            // ->add('score', null, [
            //     'label' => 'Note des spectateurs',
            //     'disabled' => true
            // ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Film::class,
        ]);
    }
}
