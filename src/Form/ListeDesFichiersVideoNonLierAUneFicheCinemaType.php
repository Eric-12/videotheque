<?php

namespace App\Form;

use App\Entity\VideoFile;
use App\Repository\VideoFileRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ListeDesFichiersVideoNonLierAUneFicheCinemaType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('nom',EntityType::class,[
                'label' => false,
                'label_attr' => ['class' => 'text-light'],
                'attr' => ['size' => '10'],
                // Défini la table
                'class' => VideoFile::class,
                // Défini le champs pour afficher les valeurs sous forme d'item dans la liste déroulante.
                'choice_label' => 'nom',
                // Défini le champs pour afficher les valeurs sous forme d'ientifiant dans la liste déroulante.
                'choice_value' => 'id',
                // Gérer la selection des items.
                // 'multiple' => true,
                // 'expanded' => true,
                // REQUETE SQL PERSONALISEE
                'query_builder' => function (VideoFileRepository $er) {
                    return $er->createQueryBuilder('u')
                    ->where('u.film IS NULL')
                    ->orderBy('u.nom', 'ASC');
                },
            ])
            ->add('save',SubmitType::class,[
                'row_attr' => ['class' => 'd-flex flex-column'],
                'label' => 'Rechercher les fiches cinéma qui peuvent correspondre.',
                'attr' => ['class' => 'btn btn-outline-light'],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
             // Configure your form options here
        ]);
    }
}
