<?php

namespace App\Form;

use App\Entity\VideoFile;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class VideoFileType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('nom', null, ['label'=>'Titre du film'])
            ->add('path', null, ['label'=>'Emplacement'])
            // ->add('filename', null, ['label'=>'Nom du fichier', 'disabled'=> true])
            ->add('taille', null, ['label'=>'Taille en octets', 'disabled'=> true])
            // ->add('film', CollectionType::class, [
            //     // 'data_class' => null,
            //     'label' => 'Fiche cinéma (collection)',
            //     'entry_type' => FicheCineType::class,
            //     'entry_options' => ['label' => false,],
            //     // 'prototype_options'  => [
            //     //     'help' => 'You can enter a new name here.',
            //     // ],
            //     // 'prototype' => true,
            //     // 'prototype_data' => 'New Tag Placeholder',
            //     'allow_add' => true,
            //     // 'allow_delete' => true,
            // ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => VideoFile::class,
        ]);
    }
}
