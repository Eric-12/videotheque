<?php

namespace App\Form;

use App\Entity\Film;
use App\Repository\FilmRepository;
use App\Repository\VideoFileRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class FicheCineMatchAvecFichierVideoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('ficheCinema', EntityType::class, [
                // Sélectionne la table
                'class' => Film::class,
                'label' => false,
                // Affiche les valeurs de champs
                'choice_label' => 'titre',
                // Valeurs renvoyés lors de la sélection de l'item
                'choice_value' => 'id',
                // REQUETE SQL PERSONALISEE
                'query_builder' => function (FilmRepository $er) use($options) {
                    return $er->createQueryBuilder('film')
                    ->where('film.titre like :title')
                    // ->orWhere('film.titreOriginal like :title')
                    ->setParameter('title', '%'.$options['data'].'%')
                    ->orderBy('film.titre', 'ASC')
                    ;
                },
                'multiple' => true,
                'expanded' => true,  
            ])
            
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
