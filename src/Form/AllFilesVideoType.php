<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class AllFilesVideoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('dir', HiddenType::class, [
                'data' => $options['data']['dir'],
                'mapped' => false
            ])
            ->add('path', ChoiceType::class, [
                'label' => false,
                'choices' => $options['data']['listeDesFilms'],
                'choice_label' => function ($choice, $key, $value) {
                    return $value;
                },
                // Gérer la selection des items.
                'multiple' => true,
                'expanded' => true,
            ])
            ->add('save', SubmitType::class, [
                'row_attr' => ['class' => 'd-flex flex-column mt-4'],
                'attr' => ['class' => 'btn btn-outline-light'],
                'label' => 'Sauvegarder'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
