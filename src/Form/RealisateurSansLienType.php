<?php

namespace App\Form;

use App\Entity\Realisateur;
use App\Repository\RealisateurRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RealisateurSansLienType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('filter',EntityType::class,[
                'attr' => ['style' => 'max-height:400px; overflow-y:scroll;'],
                //Sélectionne la table
                'class' => Realisateur::class,
                'label' => false,
                // Affiche les valeurs de champs
                'choice_label' => 'nom',
                // Valeurs renvoyés lors de la sélection de l'item
                'choice_value' => 'id',
                'query_builder' => function (RealisateurRepository $er) {
                    return $er
                            ->createQueryBuilder('realisateur')
                            ->join('realisateur.films', 'films')
                            // ->where('films.titre IS NULL')
                            // ->setParameter('titre', 'Le Facteur sonne toujours deux fois')
                            ;
                },
                'multiple' => true,
                'expanded' => true,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            // 'data_class' => Realisateur::class,
        ]);
    }
}
