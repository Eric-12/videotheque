<?php

namespace App\Form;

use App\Entity\Film;
use App\Entity\Acteur;
use App\Entity\Realisateur;
use App\Repository\FilmRepository;
use App\Repository\ActeurRepository;
use Symfony\Component\Form\AbstractType;
use App\Repository\RealisateurRepository;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class FicheCineDoublonType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('allSelected',CheckboxType::class,[
                'label' => 'Tout cocher',
                'row_attr' => ['class' =>'d-flex flex-column text-bg-light p-2 mb-3'],
                'required' => false,
            ])
            ->add('titre', EntityType::class, [ 
                'row_attr' => ['class' => 'border border-secondary mb-3 p-3'],
                'attr' => ['style' => 'max-height:400px; overflow-y:scroll;'],
                //Sélectionne la table
                'class' => Film::class,
                'label' => false,
                // Affiche les valeurs de champs
                'choice_label' => 'titre',
                // Valeurs renvoyés lors de la sélection de l'item
                'choice_value' => 'id',
                // REQUETE SQL PERSONALISEE
                'query_builder' => function (FilmRepository $er) {
                    return $er->createQueryBuilder('film')
                            ->groupBy('film.titre')
                            ->having('COUNT(film.titre) > 1')
                            ->orderBy('film.titre', 'ASC')
                    ;
                },
                'multiple' => true,
                'expanded' => true,
            ])
            ->add('realisateur', EntityType::class, [ 
                'row_attr' => ['class' => 'border border-secondary mb-3 p-3'],
                'attr' => ['style' => 'max-height:400px; overflow-y:scroll;'],
                //Sélectionne la table
                'class' => Realisateur::class,
                'label' => false,
                // Affiche les valeurs de champs
                'choice_label' => 'nom',
                // Valeurs renvoyés lors de la sélection de l'item
                'choice_value' => 'id',
                // REQUETE SQL PERSONALISEE
                'query_builder' => function (RealisateurRepository $er) {
                    return $er->createQueryBuilder('realisateur')
                            ->groupBy('realisateur.nom')
                            ->having('COUNT(realisateur.nom) > 1')
                            ->orderBy('realisateur.nom', 'ASC')
                    ;
                },
                'multiple' => true,
                'expanded' => true,
            ])
            ->add('acteur', EntityType::class, [ 
                'row_attr' => ['class' => 'border border-secondary mb-3 p-3'],
                'attr' => ['style' => 'max-height:400px; overflow-y:scroll;'],
                //Sélectionne la table
                'class' => Acteur::class,
                'label' => false,
                // Affiche les valeurs de champs
                'choice_label' => 'nom',
                // Valeurs renvoyés lors de la sélection de l'item
                'choice_value' => 'id',
                // REQUETE SQL PERSONALISEE
                'query_builder' => function (ActeurRepository $er) {
                    return $er->createQueryBuilder('acteur')
                            ->groupBy('acteur.nom')
                            ->having('COUNT(acteur.nom) > 1')
                            ->orderBy('acteur.nom', 'ASC')
                    ;
                },
                'multiple' => true,
                'expanded' => true,
            ])
            ->add('delete', SubmitType::class,[
                'label' => 'Supprimer',
                'row_attr' => ['class' => 'd-flex flex-column'],
                'attr' => ['class' => 'btn btn-warning'],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
