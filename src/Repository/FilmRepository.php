<?php

namespace App\Repository;

use App\Entity\Film;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Film>
 *
 * @method Film|null find($id, $lockMode = null, $lockVersion = null)
 * @method Film|null findOneBy(array $criteria, array $orderBy = null)
 * @method Film[]    findAll()
 * @method Film[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FilmRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Film::class);
    }

    public function save(Film $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Film $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * @return Film[] Returns an array of Film objects
     */
    public function findByTitle(string $title)
    {
        return $this->createQueryBuilder('film')
            ->where('film.titre like :title')
            ->orWhere('film.titreOriginal like :title')
            ->setParameter('title', '%'.$title.'%')
            ->orderBy('film.titre', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return Film[] Returns an array of Film objects
     */ 
    public function findByFilter($date, $score, $duree, $genre, $realisateur, $acteur)
    {
        $query = $this->createQueryBuilder('film');
        if($genre && count($genre) > 0) 
        {
            $query->join('film.genre', 'genre');
            foreach ($genre as $key => $entity) 
            {
                $key == 0 ? $query->andWhere('genre = :genreValue'.$key) : $query->orWhere('genre = :genreValue'.$key);
                $query->setParameter('genreValue'.$key, $entity);
            }
        }
        if($realisateur && $realisateur != null) {
            $query->join('film.realisateur', 'realisateur')
                  ->andWhere('realisateur = :realisateurValue')
                  ->setParameter('realisateurValue', $realisateur);
        }
        // if($realisateur && count($realisateur) > 0) {
        //     $query->join('film.realisateur', 'realisateur');
        //     foreach ($realisateur as $key => $entity) 
        //     {
        //         $key == 0 ? $query->andWhere('realisateur = :realisateurValue'.$key) : $query->orWhere('realisateur = :realisateurValue'.$key);
        //         $query->setParameter('realisateurValue'.$key, $entity);
        //     }
        // }
        if($acteur && $acteur != null) {
            $query  ->join('film.acteur', 'acteur')
                    ->andWhere('acteur = :acteurValue')
                    ->setParameter('acteurValue', $acteur);
        }
        // if($acteur && count($acteur) > 0) 
        // {
        //     $query->join('film.acteur', 'acteur');
        //     foreach ($acteur as $key => $entity) 
        //     {
        //         $key == 0 ? $query->andWhere('acteur = :acteurValue'.$key) : $query->orWhere('acteur = :acteurValue'.$key);
        //         $query->setParameter('acteurValue'.$key, $entity);
        //     }
        // }
        if($date != "" && $date !="1899|".date("Y")) 
        {
            $date = explode("|", $date);
            /* Notice php by default assume the give string as such format:
                '-'    is    'y-m-d'
                '/'    is    'm/d/y' */
            $dateMin = date_create('01/01/'.$date[0]);
            $dateMax = date_create('12/31/'.$date[1]);
            if (!$dateMax) $dateMax = date_create();
            $query
                ->andWhere('film.date BETWEEN :dateMin AND :dateMax')
                ->setParameter('dateMin',$dateMin)
                ->setParameter('dateMax',$dateMax);
        }
        if($score != "" && $score != "0|5") 
        {
            $score = explode("|", $score);
            $query
                ->andWhere('film.score BETWEEN :scoreMin AND :scoreMax')
                ->setParameter('scoreMin',$score[0])
                ->setParameter('scoreMax',$score[1]);
                // ->orderBy('film.score', 'DESC');
        }
        if($duree != "" && $duree != "1|255") 
        {
            $duree = explode("|", $duree);
            $query
                ->andWhere('film.duree BETWEEN :dureeMin AND :dureeMax')
                ->setParameter('dureeMin', $duree[0])
                ->setParameter('dureeMax', $duree[1]);

        }
        $query
            ->orderBy('film.titre', 'ASC')
            ->getQuery()
            ->getResult();

        return $query;
    }

}
