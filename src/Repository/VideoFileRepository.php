<?php

namespace App\Repository;

use App\Entity\VideoFile;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<VideoFile>
 *
 * @method VideoFile|null find($id, $lockMode = null, $lockVersion = null)
 * @method VideoFile|null findOneBy(array $criteria, array $orderBy = null)
 * @method VideoFile[]    findAll()
 * @method VideoFile[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VideoFileRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, videoFile::class);
    }

    public function save(VideoFile $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(VideoFile $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * @return VideoFile[] Returns an array of Film objects
     */
    public function findByFileName(string $title)
    {
        return $this->createQueryBuilder('filePath')
            ->where('filePath.nom like :title')
            ->setParameter('title', '%'.$title.'%')
            ->orderBy('filePath.nom', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }

}
