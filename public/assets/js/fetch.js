
export const get = async (url, credential) => {
    // CREATION DE L'ENTETE DE LA REQUETE
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    const headersOptions = {
        method: 'GET',
        headers: headers,
    };
    return fetch(process.env.REACT_APP_API_BASE_URL + url, headersOptions)
        .then(response => response.json())
        .then(
            (result) => {

                if (result.code === 401) {
                    return ({ error: result })
                }
                return result;
            },
            (error) => {

            }
        )
}

export const post = async (url, credential, body) => {
    // CREATION DE L'ENTETE DE LA REQUETE
    const headers = new Headers();
    headers.append('Content-Type', 'application/ld+json');
    const headersOptions = {
        method: 'POST',
        headers: headers,
        body: JSON.stringify(body)
    };

    return fetch(process.env.REACT_APP_API_BASE_URL + url, headersOptions)
        .then(response => response.json())
        .then(
            (result) => {
                // Si l'api remonte une erreur
                if (result.code) {
                    return { error: result }
                }
                return result;
            },
            (error) =>{
                console.log(error.message)
            }
        );
}

export const put = async (url, credential, body) => {
    // CREATION DE L'ENTETE DE LA REQUETE
    const headers = new Headers();
    headers.append('Content-Type', 'application/ld+json');
    const headersOptions = {
        method: 'PUT',
        headers: headers,
        body: JSON.stringify(body)
    };

    return fetch(process.env.REACT_APP_API_BASE_URL + url, headersOptions)
        .then(response => response.json())
        .then((data) => {
            return data;
        });
}

