
const loaderComponent = () => {
    return (
        `<div class="loader">
            <span class="loader__letter">E</span>
            <span class="loader__letter">N</span>
            <span class="loader__letter">C</span>
            <span class="loader__letter">O</span>
            <span class="loader__letter">D</span>
            <span class="loader__letter">A</span>
            <span class="loader__letter">G</span>
            <span class="loader__letter">E</span>
            <span class="loader__letter">.</span>
            <span class="loader__letter">E</span>
            <span class="loader__letter">N</span>
            <span class="loader__letter">.</span>
            <span class="loader__letter">C</span>
            <span class="loader__letter">O</span> 
            <span class="loader__letter">U</span> 
            <span class="loader__letter">R</span> 
            <span class="loader__letter">S</span> 
            <span class="loader__letter">.</span> 
            <span class="loader__letter">.</span> 
            <span class="loader__letter">.</span> 
        </div>`
    )
}
