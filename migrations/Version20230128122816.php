<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230128122816 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8244BE22FF7747B4 ON film (titre)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_835033F86C6E55B5 ON genre (nom)');
        $this->addSql('DROP INDEX UNIQ_8B086BCCB548B0F ON video_file');
        $this->addSql('ALTER TABLE video_file ADD root VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP INDEX UNIQ_8244BE22FF7747B4 ON film');
        $this->addSql('DROP INDEX UNIQ_835033F86C6E55B5 ON genre');
        $this->addSql('ALTER TABLE video_file DROP root');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8B086BCCB548B0F ON video_file (path)');
    }
}
